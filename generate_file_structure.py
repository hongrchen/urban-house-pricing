import os
import sys
from os.path import join, isdir, isfile
from glob import glob

subdirs = glob(os.getcwd(), recursive = False)
allFiles = glob(os.getcwd(), recursive = True)

#os.chdir('C:/Users/otomr/OneDrive/Documents/GitHub/UrbanHousing-xai-iml22')
sys.stdout = open(join(os.getcwd(), 'file_structure.txt'), 'w')

wanted_files = ['.csv', '.js', '.ts', '.json', '.md', '.txt', '.py']
unwanted_files = ['.pyc']

def print_cur_dir(level, dir):
    for x in os.listdir(join(os.getcwd(), dir)):
        if 'node_modules' not in x:
            if isdir(join(dir, x)):
                print(level*'|-- ' + x)
                print_cur_dir(level+1, join(dir, x))
            elif any(ft in x for ft in wanted_files) and not any(ft in x for ft in unwanted_files):
                print(level*'|-- ' + x)

# Print only backend and frontend folders
print('|-- ' + 'backend-project')
print_cur_dir(2, 'backend-project')
print('|-- ' + 'react-frontend')
print_cur_dir(2, 'react-frontend')

# Print base directory structure
for file in os.listdir(os.getcwd()):
    if isfile(file):
        if any(ft in file for ft in wanted_files):
            print('|-- ' + file)

