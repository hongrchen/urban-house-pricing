# House Price Developement in the US

[[_TOC_]]


## Project Description 

In this project we will design an interactive tool for comparing the prices and their development of real estate across states in the US as well as within some of their cities. 
We will use both pricing data over time as well as socio-economic indicators over time to predict the prices in the future.
Our tool should help advise the user which state/city it is most profitable to invest in.


### Users

The traget user is a business investor that wants to buy real estate properties as a personal investment. 

### Datasets
<!-- Add here all used datasets.\
Document here where to find the data and how to download it.  -->
Here the sources of all used data sets are documented. For usage these data set have been preprocessed to fit the need of the envisioned task.

In summary, data on house prices and socio-economical properties of the US in different granularity is collected for as many years as possible from 2000 to 2021.



Data Sources:

- House price data: https://www.zillow.com/
- Income Wealth and Asset data: https://www.census.gov/topics/income-poverty/income/data/tables.All.List_1734169494.html
- US State to Region mapping: https://github.com/cphalpert/census-regions/blob/master/us%20census%20bureau%20regions%20and%20divisions.csv
- Population per city: https://www.census.gov/programs-surveys/popest/technical-documentation/research/evaluation-estimates/2020-evaluation-estimates/2010s-cities-and-towns-total.html
- Unemploment at city level: https://data.census.gov/cedsci/ (combination of various tables)
- Bulding Permits Dataset: https://www.census.gov/construction/bps/msaannual.html
- GDP dataset: https://apps.bea.gov/itable/iTable.cfm?ReqID=70&step=1&acrdn=1
- Lending Intrest Rate dataset:	https://data.worldbank.org/indicator/FR.INR.LEND?locations=US

What data is collected:

<!--Maybe create a table Property/ granularity/ timespan (years available)/ model used to predict missing years/ model to predict the future years-/ source link-->
- House Price on city level/state level (2000 - 2021)
- Population (2000 - 2021)
- Unemployment (2016 - 2020)
- annualy GDP of US states (2005 - 2021)
- Lending Interest Rate for the US (2000 - 2021)
- Building Permits for US core-based statistical areas (CBSAs) (2004 - 2019)


### Tasks
<!-- Define all the tasks you want your dashboard solve. -->

1. Predict house price developement in future years.
2. Advise the user which state it is most profitable to invest in.
3. Within the chosen state: identify which city it is most profitable to invest in.
4. Allow the user to make custom predictions of the prices based on alternative indicator values in the future.
...

- - -
## Folder Structure
 <!-- Specify here the structure of you code and comment what the most important files contain  -->

Here the structure of the code is specified. For the most important files it is commented what they contain.

``` bash
|-- backend-project
|-- |-- .ipynb_checkpoints
|-- |-- aggregate_state_data.py
|-- |-- app.py
|-- |-- city_data
|-- |-- |-- Abilene.csv
|-- |-- |-- Akron.csv
|-- |-- |-- Alameda.csv
|-- |-- |-- Alameda_dynamic.csv
|-- |-- |-- Albany.csv
|-- |-- |-- Albuquerque.csv
|-- |-- |-- Alexandria.csv
|-- |-- |-- Allentown.csv
|-- |-- |-- Ames.csv
|-- |-- |-- Anaheim.csv
|-- |-- |-- Ann Arbor.csv
|-- |-- |-- Apopka.csv
|-- |-- |-- Apple Valley.csv
|-- |-- |-- Appleton.csv
|-- |-- |-- Arlington.csv
|-- |-- |-- Arlington_dynamic.csv
|-- |-- |-- Arvada.csv
|-- |-- |-- Atlanta.csv
|-- |-- |-- Auburn.csv
|-- |-- |-- Aurora.csv
|-- |-- |-- Austin.csv
|-- |-- |-- Austin_dynamic.csv
|-- |-- |-- Avondale.csv
|-- |-- |-- Bakersfield.csv
|-- |-- |-- Baltimore.csv
|-- |-- |-- Baltimore_dynamic.csv
|-- |-- |-- Bartlett.csv
|-- |-- |-- Baton Rouge.csv
|-- |-- |-- Beaverton.csv
|-- |-- |-- Beaverton_dynamic.csv
|-- |-- |-- Bellevue.csv
|-- |-- |-- Bellingham.csv
|-- |-- |-- Billings.csv
|-- |-- |-- Biloxi.csv
|-- |-- |-- Birmingham.csv
|-- |-- |-- Bloomington.csv
|-- |-- |-- Boston.csv
|-- |-- |-- Bridgeport.csv
|-- |-- |-- Buffalo.csv
|-- |-- |-- Camden.csv
|-- |-- |-- Chandler.csv
|-- |-- |-- Charleston.csv
|-- |-- |-- Charlotte.csv
|-- |-- |-- Charlottesville.csv
|-- |-- |-- Chesapeake.csv
|-- |-- |-- Chesterfield.csv
|-- |-- |-- Chicago.csv
|-- |-- |-- Chicago_dynamic.csv
|-- |-- |-- Chula Vista.csv
|-- |-- |-- Cincinnati.csv
|-- |-- |-- Cleveland.csv
|-- |-- |-- Colorado Springs.csv
|-- |-- |-- Columbia.csv
|-- |-- |-- Conway.csv
|-- |-- |-- Corpus Christi.csv
|-- |-- |-- Covington.csv
|-- |-- |-- Dallas.csv
|-- |-- |-- Dallas_dynamic.csv
|-- |-- |-- Denver.csv
|-- |-- |-- Des Moines.csv
|-- |-- |-- Detroit.csv
|-- |-- |-- Detroit_dynamic.csv
|-- |-- |-- Durham.csv
|-- |-- |-- East Providence.csv
|-- |-- |-- El Paso.csv
|-- |-- |-- Fargo.csv
|-- |-- |-- Fayetteville.csv
|-- |-- |-- Fontana.csv
|-- |-- |-- Fort Wayne.csv
|-- |-- |-- Fort Worth.csv
|-- |-- |-- Fort Worth_dynamic.csv
|-- |-- |-- Fremont.csv
|-- |-- |-- Fremont_dynamic.csv
|-- |-- |-- Fresno.csv
|-- |-- |-- Garland.csv
|-- |-- |-- Garland_dynamic.csv
|-- |-- |-- Grand Rapids.csv
|-- |-- |-- Greensboro.csv
|-- |-- |-- Henderson.csv
|-- |-- |-- Houston.csv
|-- |-- |-- Houston_dynamic.csv
|-- |-- |-- Huntington Beach.csv
|-- |-- |-- Huntington Beach_dynamic.csv
|-- |-- |-- Huntsville.csv
|-- |-- |-- Indianapolis.csv
|-- |-- |-- Irvine.csv
|-- |-- |-- Irvine_dynamic.csv
|-- |-- |-- Irving.csv
|-- |-- |-- Irving_dynamic.csv
|-- |-- |-- Jacksonville.csv
|-- |-- |-- Jersey City.csv
|-- |-- |-- Kansas City.csv
|-- |-- |-- Laredo.csv
|-- |-- |-- Las Vegas.csv
|-- |-- |-- Lincoln.csv
|-- |-- |-- Little Rock.csv
|-- |-- |-- Logan.csv
|-- |-- |-- Long Beach.csv
|-- |-- |-- Los Angeles.csv
|-- |-- |-- Los Angeles_dynamic.csv
|-- |-- |-- Lubbock.csv
|-- |-- |-- Manchester.csv
|-- |-- |-- Memphis.csv
|-- |-- |-- Mesa.csv
|-- |-- |-- Miami.csv
|-- |-- |-- Miami_dynamic.csv
|-- |-- |-- Midwest City.csv
|-- |-- |-- Milwaukee.csv
|-- |-- |-- Minneapolis.csv
|-- |-- |-- Moreno Valley.csv
|-- |-- |-- Nampa.csv
|-- |-- |-- New Orleans.csv
|-- |-- |-- New Orleans_dynamic.csv
|-- |-- |-- New York.csv
|-- |-- |-- New York_dynamic.csv
|-- |-- |-- Newark.csv
|-- |-- |-- Norfolk.csv
|-- |-- |-- North Las Vegas.csv
|-- |-- |-- Oakland.csv
|-- |-- |-- Oklahoma City.csv
|-- |-- |-- Oklahoma City_dynamic.csv
|-- |-- |-- Olathe.csv
|-- |-- |-- Omaha.csv
|-- |-- |-- Orlando.csv
|-- |-- |-- Orlando_dynamic.csv
|-- |-- |-- Oxnard.csv
|-- |-- |-- Philadelphia.csv
|-- |-- |-- Phoenix.csv
|-- |-- |-- Phoenix_dynamic.csv
|-- |-- |-- Pittsburgh.csv
|-- |-- |-- Plano.csv
|-- |-- |-- Portland.csv
|-- |-- |-- Raleigh.csv
|-- |-- |-- Reno.csv
|-- |-- |-- Riverside.csv
|-- |-- |-- Sacramento.csv
|-- |-- |-- Sacramento_dynamic.csv
|-- |-- |-- Salt Lake City.csv
|-- |-- |-- San Antonio.csv
|-- |-- |-- San Antonio_dynamic.csv
|-- |-- |-- San Bernardino.csv
|-- |-- |-- San Diego.csv
|-- |-- |-- San Francisco.csv
|-- |-- |-- San Francisco_dynamic.csv
|-- |-- |-- San Jose.csv
|-- |-- |-- Santa Ana.csv
|-- |-- |-- Santa Clarita.csv
|-- |-- |-- Scottsdale.csv
|-- |-- |-- Seattle.csv
|-- |-- |-- Sioux Falls.csv
|-- |-- |-- Spokane.csv
|-- |-- |-- Stockton.csv
|-- |-- |-- Tacoma.csv
|-- |-- |-- Tampa.csv
|-- |-- |-- Tampa_dynamic.csv
|-- |-- |-- Tempe.csv
|-- |-- |-- Toledo.csv
|-- |-- |-- Tucson.csv
|-- |-- |-- Tulsa.csv
|-- |-- |-- Virginia Beach.csv
|-- |-- |-- Washington.csv
|-- |-- |-- Wichita.csv
|-- |-- |-- Winston-Salem.csv
|-- |-- |-- Yonkers.csv
|-- |-- city_models
|-- |-- data
|-- |-- |-- BuildingPermits_US_merged_2004_to_2019.csv
|-- |-- |-- city_coords.csv
|-- |-- |-- counties_prices_2022.csv
|-- |-- |-- GDP_US_byState_annualy_2005_2021.csv
|-- |-- |-- generate_data.py
|-- |-- |-- InterestRate_2000_2021.csv
|-- |-- |-- LendingInterestRate_US_2000_2021.csv
|-- |-- |-- merged_data.csv
|-- |-- |-- merged_data_extended.csv
|-- |-- |-- pop00_10.csv
|-- |-- |-- pop10_20.csv
|-- |-- |-- pop20_21.csv
|-- |-- |-- price_by_city.csv
|-- |-- |-- RegionData.csv
|-- |-- |-- states_prices_2022.csv
|-- |-- |-- State_Region.csv
|-- |-- |-- Unemp16.csv
|-- |-- |-- Unemp17.csv
|-- |-- |-- Unemp18.csv
|-- |-- |-- Unemp19.csv
|-- |-- |-- Unemp20.csv
|-- |-- Dynamic_price_prediction.csv
|-- |-- merge_state_data.py
|-- |-- model.py
|-- |-- predict.py
|-- |-- pred_full_data.csv
|-- |-- pydantic_models
|-- |-- |-- __pycache__
|-- |-- state_data
|-- |-- |-- Alabama.csv
|-- |-- |-- Alaska.csv
|-- |-- |-- all_states.csv
|-- |-- |-- Arizona.csv
|-- |-- |-- Arkansas.csv
|-- |-- |-- California.csv
|-- |-- |-- Colorado.csv
|-- |-- |-- Connecticut.csv
|-- |-- |-- District of Columbia.csv
|-- |-- |-- Florida.csv
|-- |-- |-- Georgia.csv
|-- |-- |-- Hawaii.csv
|-- |-- |-- Idaho.csv
|-- |-- |-- Illinois.csv
|-- |-- |-- Indiana.csv
|-- |-- |-- Iowa.csv
|-- |-- |-- Kansas.csv
|-- |-- |-- Kentucky.csv
|-- |-- |-- Louisiana.csv
|-- |-- |-- Maryland.csv
|-- |-- |-- Massachusetts.csv
|-- |-- |-- Michigan.csv
|-- |-- |-- Minnesota.csv
|-- |-- |-- Mississippi.csv
|-- |-- |-- Missouri.csv
|-- |-- |-- Montana.csv
|-- |-- |-- Nebraska.csv
|-- |-- |-- Nevada.csv
|-- |-- |-- New Hampshire.csv
|-- |-- |-- New Jersey.csv
|-- |-- |-- New Mexico.csv
|-- |-- |-- New York.csv
|-- |-- |-- North Carolina.csv
|-- |-- |-- North Dakota.csv
|-- |-- |-- Ohio.csv
|-- |-- |-- Oklahoma.csv
|-- |-- |-- Oregon.csv
|-- |-- |-- Pennsylvania.csv
|-- |-- |-- Rhode Island.csv
|-- |-- |-- South Carolina.csv
|-- |-- |-- South Dakota.csv
|-- |-- |-- Tennessee.csv
|-- |-- |-- Texas.csv
|-- |-- |-- Utah.csv
|-- |-- |-- Virginia.csv
|-- |-- |-- Washington.csv
|-- |-- |-- West Virginia.csv
|-- |-- |-- Wisconsin.csv
|-- |-- Static_price_prediction.csv
|-- |-- Static_price_prediction_change.csv
|-- |-- Static_price_prediction_max.csv
|-- |-- Static_price_prediction_min.csv
|-- |-- __pycache__
|-- react-frontend
|-- |-- app
|-- |-- |-- package-lock.json
|-- |-- |-- package.json
|-- |-- |-- public
|-- |-- |-- |-- manifest.json
|-- |-- |-- |-- robots.txt
|-- |-- |-- |-- states-10m.json
|-- |-- |-- README.md
|-- |-- |-- src
|-- |-- |-- |-- App.js
|-- |-- |-- |-- App.test.js
|-- |-- |-- |-- backend
|-- |-- |-- |-- |-- BackendQueryEngine.js
|-- |-- |-- |-- |-- BackendQueryEngineError.js
|-- |-- |-- |-- |-- json-decoder.ts
|-- |-- |-- |-- ChartsComponent.js
|-- |-- |-- |-- Collapsible1Component.js
|-- |-- |-- |-- Collapsible2Component.js
|-- |-- |-- |-- Collapsible3Component.js
|-- |-- |-- |-- components
|-- |-- |-- |-- |-- libraries
|-- |-- |-- |-- |-- |-- BarChart.js
|-- |-- |-- |-- |-- |-- Data.js
|-- |-- |-- |-- |-- |-- LineChart.js
|-- |-- |-- |-- |-- |-- LineChartSelection.js
|-- |-- |-- |-- |-- |-- LineChartSelectionProp.js
|-- |-- |-- |-- |-- |-- PieChart.js
|-- |-- |-- |-- |-- |-- price_by_city.js
|-- |-- |-- |-- DynamicComponent.js
|-- |-- |-- |-- GeoChart.world.geo.json
|-- |-- |-- |-- GeoComponent.js
|-- |-- |-- |-- GeoWrapper.js
|-- |-- |-- |-- index.js
|-- |-- |-- |-- reportWebVitals.js
|-- |-- |-- |-- setupTests.js
|-- |-- |-- |-- types
|-- |-- |-- |-- |-- DataArray.ts
|-- |-- |-- |-- |-- DataPoint.ts
|-- |-- |-- |-- |-- Margins.ts
|-- |-- |-- |-- useDidMountEffect.js
|-- |-- |-- |-- useResizeObserver.js
|-- |-- package-lock.json
|-- file_structure.txt
|-- generate_file_structure.py
|-- package-lock.json
|-- README.md
|-- requirements.txt

```

## Requirements
Here all requirements to build the environment and run the code ar elisted in requirements.txt.



## How to Run

To run backend:

  ```cd backend-project```

  ```uvicorn app:app --reload```

In another powershell: (to run frontend)

  ```cd react-frontend/app```

  ```npm install```
  
  ```npm start```

No need to use docker.





