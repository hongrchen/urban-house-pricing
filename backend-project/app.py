from hashlib import sha1
from multiprocessing.spawn import old_main_modules
from turtle import shape
from fastapi import FastAPI, UploadFile, File, HTTPException
from fastapi.responses import HTMLResponse
from fastapi.middleware.cors import CORSMiddleware

import uvicorn
import pandas as pd
import numpy as np
import os
import csv
import codecs
from scipy import interpolate
from io import StringIO
from typing import Callable
from predict import predict

app = FastAPI(
    title="Test Python Backend",
    description="""This is a template for a Python backend.
                   It provides acess via REST API.""",
    version="0.1.0",
)

# Allow CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post("/get-cities")
def get_cities(name="Texas",year=2022,property="Price"):
    year=int(year)
    pred_full_data=pd.read_csv(f"pred_full_data.csv")

    if property=='PriceChange':

        property='Price'

        pred_full_data2022=pred_full_data.loc[(pred_full_data.State==name) & (pred_full_data.Year==2022),['City',property]]
        pred_full_data=pred_full_data.loc[(pred_full_data.State==name) & (pred_full_data.Year==year),['City',property]]

        diff=[]
        for i,j in zip(pred_full_data[property],pred_full_data2022[property]):
            diff.append(round((i/j)*100))

        pred_full_data.insert(2,"PriceChange",diff)

        pred_full_data=pred_full_data[['City', 'PriceChange']]

        # renameCollumns
        pred_full_data.columns = ['City', 'value']

    else:
        pred_full_data=pred_full_data.loc[(pred_full_data.State==name) & (pred_full_data.Year==year),['City',property]].rename(columns={property:'value'})
        pred_full_data['value']=pred_full_data['value'].fillna(0)

    return pred_full_data.to_dict(orient="records")

@app.post("/get-placeholders")
def get_placeholders(city="Texas",year=2022):
    pred_full_data=pd.read_csv(f"pred_full_data.csv")
    year=int(year)
    pred_full_data=pred_full_data.loc[(pred_full_data.City==city) & (pred_full_data.Year==year),['City',
    'Year','Population','Unemployment','GDP','Building Permits','Interest rate']]
    pred_full_data=pred_full_data.fillna('')
    #print(pred_full_data.head())

    return pred_full_data.to_dict(orient="records")

@app.post("/get-data")
def upload_data(year, pro):
    #print('Pro:',pro)

    # get data from all_state data file
    all_states=pd.read_csv(f"state_data/all_states.csv")
    # print(all_states)

    if pro=='PriceChange':
        # get prices
        pro='Price'

        # filter out reference year for percentage change (here 2022)
        all_states_2022=all_states.loc[all_states['Year'] == int(2022)]

        # filter by selected year
        all_states=all_states.loc[all_states['Year'] == int(year)]

        # filter by selected property
        all_states=all_states[['State', pro]]
        all_states_2022=all_states_2022[['State', pro]]

        diff=[]
        for i,j in zip(all_states[pro],all_states_2022[pro]):
            diff.append(round((i/j)*100))

        all_states.insert(2,"PriceChange",diff)

        all_states=all_states[['State', 'PriceChange']]

        # drop rows with no data (NaN)
        all_states=all_states.dropna()

    else:
        # filter by selected year
        all_states=all_states.loc[all_states['Year'] == int(year)]

        # filter by selected property
        all_states=all_states[['State', pro]]

        # drop rows with no data (NaN)
        all_states=all_states.dropna()

    # rename columns
    all_states.columns = ['RegionName', 'value']

    return all_states.to_dict(orient="records")

# get-minVal

@app.post("/get-minVal")
def upload_data(year, pro):

    # get data from all_states data file
    all_states=pd.read_csv(f"state_data/all_states.csv")
    print(all_states)

    if pro=='PriceChange':
        minVal=10
        
    else:
        # filter by selected property
        minVal=all_states[pro].min()*0.3
        if minVal==0:
            minVal=0

    print(minVal)
    return minVal

@app.post("/get-maxVal")
def upload_data(year, pro):

    # get data from all_state data file
    all_states=pd.read_csv(f"state_data/all_states.csv")
    print(all_states)

    if pro=='PriceChange':
        maxVal=260
        
    else:
        # filter by selected property
        maxVal=all_states[pro].max()*0.8

    print(maxVal)
    return maxVal

@app.post("/get-properties")
def upload_dataProp(pro, city):

    # get city data for selected city
    city_data=pd.read_csv(f"city_data/{city}.csv")
    # print(city_data.head())

    # filter by selected property
    city_data=city_data[['Year', pro, '%s min'%pro, '%s max'%pro]]  #.rename(columns={pro:'property'})
    # print(city_data.head())

    return city_data.to_dict(orient="records")

# Obsolete?
@app.post("/get-full-predictions")
def get_full_predictions(socio_index:str):
    pred_full_data=pd.read_csv(f"pred_full_data.csv")
    data_pivot = pd.pivot_table(pred_full_data, index='City', columns='Year', values=socio_index)
    data_pivot = data_pivot.loc[:,socio_index]
    return data_pivot.to_dict(orient="records")

@app.post("/get-prices")
def get_prices():
    data=pd.read_csv(f"Static_price_prediction.csv")
    datamin=pd.read_csv(f"Static_price_prediction_min.csv")
    datamax=pd.read_csv(f"Static_price_prediction_max.csv")
    datachange=pd.read_csv(f"Static_price_prediction_change.csv")
    #print(data.head())
    data=pd.melt(data,id_vars='City',value_vars=data.columns[1:],value_name='price',var_name='year')
    datamin=pd.melt(datamin,id_vars='City',value_vars=datamin.columns[1:],value_name='price_min',var_name='year')
    datamax=pd.melt(datamax,id_vars='City',value_vars=datamax.columns[1:],value_name='price_max',var_name='year')
    datachange=pd.melt(datachange,id_vars='City',value_vars=datachange.columns[1:],value_name='price_change',var_name='year')
    data[['price']] = data.dropna()[['price']].astype(int)
    datamin[['price_min']] = datamin.dropna()[['price_min']].astype(int)
    datamax[['price_max']] = datamax.dropna()[['price_max']].astype(int)
    datachange[['price_change']] = datachange.dropna()[['price_change']].round(2)
    data=data.merge(datamin,on=['City','year']).merge(datamax,on=['City','year']).merge(datachange,on=['City','year'])
    data=data.loc[~data['price'].isna(),:]
    data['price_min']=data['price_min'].fillna(data['price'])
    data['price_max']=data['price_max'].fillna(data['price'])
    data['price_max_change']=(data['price_max']*data['price_change'])/(data['price'])
    data['price_min_change']=(data['price_min']*data['price_change'])/(data['price'])
    #print(data.to_numpy().max())
    #print(data.to_numpy().min())
    data.to_csv('text.csv')
    data=data.sort_values(['year','City'])
    print(data.head())
    print(data.tail())

    # Clear dynamic prices on load
    if os.path.isfile("Dynamic_price_prediction.csv"):
        os.remove("Dynamic_price_prediction.csv")

    return data.to_dict(orient="records")

# Ask for an updated price estimate
@app.post("/get-dynamic-prices")
def get_dynamic_prices(city='Chicago', year=2043, population=6000000, unemployment=4, gdp=000000, permits=200, interest=2):
    vals=[]
    vars=[]
    if population!='' and population!='undefined':
        vals.append(str(population))
        vars.append('Population')
    if unemployment!='' and unemployment!='undefined':
        vals.append(str(unemployment))
        vars.append('Unemployment')
    if gdp!='' and gdp!='undefined':
        vals.append(str(gdp))
        vars.append('GDP')
    if permits!='' and permits!='undefined':
        vals.append(str(permits))
        vars.append('Building Permits')
    if interest!='' and interest!='undefined':
        vals.append(str(interest))
        vars.append('Interest rate')
    #vals=[str(Population),str(Unemployment), str(gdp), str(permits), str(interest)]
    #print(vals)
    #vars=['Population', 'Unemployment', 'GDP', 'Building Permits', 'Interest rate']
    
    # Call model with custom input
    old_inidicators = predict(city=city, year=str(year), variables=str(vars), vals=str(vals))
    print("indicators")
    print(old_inidicators)
    
    data = pd.read_csv(f"Dynamic_price_prediction.csv")
    name = data.iloc[0,0]
    
    print("Length: " + str(len(data.index)))
    if len(data.index) > 5:
        #print(data.tail())
        old_data = data.iloc[3:]
        #print(old_data.tail())
        old_data = old_data.transpose()    
        old_data = old_data[1:]
        old_data = old_data.reset_index(level=0)
        old_data.insert(0, "City", name)
        #print(old_data.tail())
        old_data.columns = ["City", "Year", "Price", "Price min", "Price max"]
        #print(old_data.head())
    data = data.iloc[:3]
    data = data.transpose()    
    data = data[1:]
    data = data.reset_index(level=0)
    data.insert(0, "City", name)
    data.columns = ["City", "Year", "Price", "Price min", "Price max"]
    data = data.rename(columns={'Year':'year','Price':'price','Price min':'price_min','Price max':'price_max'})
    print("Here")
    print(data)

    print(vals)
    print(vars)

    to_ret = []
    for i in range(22):
        cur_dict = {}
        cur_dict['City'] = data['City'].iloc[i]
        cur_dict['year'] = data['year'].iloc[i]
        cur_dict['price'] = data['price'].iloc[i]
        to_ret.append(cur_dict)
    for i in range(22, len(data.index)):
        cur_dict = {}
        cur_dict['City'] = data['City'].iloc[i]
        cur_dict['year'] = data['year'].iloc[i]
        cur_dict['price'] = data['price'].iloc[i]
        cur_dict['price_min'] = data['price_min'].iloc[i]
        cur_dict['price_max'] = data['price_max'].iloc[i]

        # Add previous prediction
        if 'old_data' in locals():
            cur_dict['prev_price'] = old_data['Price'].iloc[i]
        
        to_ret.append(cur_dict)
        #to_ret.append(data.to_dict('records')[i])

    to_ret.append(old_inidicators)
    print(to_ret)

    return to_ret
    #return {"data": to_ret, "ind": old_inidicators}
    #return data.to_dict(orient="records")

@app.post("/files/")
async def create_file(file: bytes = File(...)):
    return {"file_size": len(file)}


@app.post("/uploadfile/")
async def create_upload_file(file: UploadFile):
    return {"filename": file.filename}


@app.get("/", response_class=HTMLResponse)
async def root():
    html_content = """
        <html>
            <head>
                <title>Week 2</title>
            </head>
            <body>
                <h1>Test Python Backend</h1>
                Visit the <a href="/docs">API doc</a> (<a href="/redoc">alternative</a>) for usage information.
            </body>
        </html>
        """
    return HTMLResponse(content=html_content, status_code=200)


def update_schema_name(app: FastAPI, function: Callable, name: str) -> None:
    """
    Updates the Pydantic schema name for a FastAPI function that takes
    in a fastapi.UploadFile = File(...) or bytes = File(...).

    This is a known issue that was reported on FastAPI#1442 in which
    the schema for file upload routes were auto-generated with no
    customization options. This renames the auto-generated schema to
    something more useful and clear.

    Args:
        app: The FastAPI application to modify.
        function: The function object to modify.
        name: The new name of the schema.
    """
    for route in app.routes:
        if route.endpoint is function:
            route.body_field.type_.__name__ = name
            break


update_schema_name(app, create_file, "CreateFileSchema")
update_schema_name(app, create_upload_file, "CreateUploadSchema")
