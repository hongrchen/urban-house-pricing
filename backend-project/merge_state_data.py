# Merge all state files together

import pandas as pd
import numpy as np
import os
from os.path import join

# bring all state files into one file 
city_state = pd.read_csv(f"backend-project/data/merged_data_extended.csv")
states = city_state['STNAME'].unique()
columns = ['Year', 'Price', 'State', 'Region', 'Population', 'Unemployment', 'GDP', 'Building Permits', 'Interest rate']

for i,state in enumerate(states):
    # singleState_data=pd.read_csv(f"backend-project/state_data/"+state+".csv")
    # #print(singleState_data.head())
    print(i)
    print(state)
    if i == 0:
        State_data=pd.read_csv(f"backend-project/state_data/"+state+".csv")
    else:
        State_data=State_data.append(pd.read_csv(f"backend-project/state_data/"+state+".csv"))
        # print(State_data.head())
        # pd.concat([State_data, clickDF], ignore_index=True, sort=True)

State_data.reset_index(drop=True)

State_data = State_data.iloc[: , 1:]
# print(State_data.head())
print(State_data.tail())
# print(State_data.shape)
# print(all_states.head())

State_data.to_csv('backend-project/state_data/all_states.csv', index=True)

print("Done")