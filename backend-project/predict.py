import numpy as np
import pandas as pd
from pandas.core.common import SettingWithCopyWarning
from sklearn import linear_model
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
from statsmodels.tsa.arima.model import ARIMAResults
from statsmodels.tsa.statespace.sarimax import SARIMAX, SARIMAXResults
import statsmodels.api as sm
from itertools import chain
from copy import copy
import warnings

import os
import sys
import ast
import math

# Set quantiles for confidence intervals
CI_LOWER = .05
CI_UPPER = 1 - CI_LOWER

def predict(city, year, variables, vals):
    warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)

    # Parse in user request
    #city = sys.argv[1]
    #year = sys.argv[2]
    variables = ast.literal_eval(variables)
    variables = [n.strip() for n in variables]
    vals = ast.literal_eval(vals)
    vals = [n.strip() for n in vals]

    print(variables)
    print(vals)

    # Load data
    data = pd.read_csv('data/merged_data_extended.csv', index_col='City')
    state_region = pd.read_csv('data/State_Region.csv')
    gdp = pd.read_csv('data/GDP_US_byState_annualy_2005_2021.csv', sep=';')
    gdp.columns = list(gdp.columns[:1]) + ['GDP'+i for i in gdp.columns[1:]]

    state_region = state_region.merge(gdp, how='left', on='State')
    data = data.reset_index().merge(state_region, how='left', left_on='STNAME', right_on='State').set_index(data.index)

    bp = pd.read_csv('data/BuildingPermits_US_merged_2004_to_2019.csv', sep=',')
    #bp = bp.iloc[:,[2]+list(range(4,bp.shape[1]))]
    #bp.columns = list(bp.columns[:1]) + ['BP'+i for i in bp.columns[1:]]
    bp = bp.iloc[:,2:]
    bp.columns = list(bp.columns[:2]) + ['BP'+i for i in bp.columns[2:]]
    data = data.merge(bp, how='left', left_on=['NAME'], right_on=['Name'])
    #data = data.merge(bp, how='left', left_on=['NAME','State Code'], right_on=['Name','State'])
    #data.drop('State_x', axis=1, inplace=True)
    #data.rename(columns={"State_y": "State"})

    #data.to_csv('TestData.csv')

    intRate = pd.read_csv('data/InterestRate_2000_2021.csv', sep=',')
    intRate.drop(labels=['Indicator'], axis=1, inplace=True)

    start_known = 2000
    end_known = 2022
    start_pred = 2023
    end_pred = int(year)
    known_time_range = range(start_known, end_known+1)
    prediction_time_range = range(start_pred, end_pred+1)
    
    indicators = ['Population', 'Unemployment', 'GDP', 'Building Permits', 'Interest rate']
    columns = ['Year', 'Price', 'Price min', 'Price max', 'State', 'Region'] + indicators
    local_indicators = copy(indicators)

    cities = [city, 'Price_min', 'Price_max']
    years = [str(i+2000) for i in range(end_pred+1 - start_known)]

    # Add old house prices
    house_prices = pd.DataFrame(np.NAN, index = cities, columns=years)

    # Load SARIMAX model
    loaded_model = SARIMAXResults.load('city_models/' + city + '.pkl')

    city_data = pd.DataFrame(np.NAN, index = range(end_pred+1 - start_known), columns=columns)
    city_data['Year'] = range(start_known, end_pred+1)
    raw_data = data[data['City']==city]

    # Duplicate cities treatment
    if len(raw_data.index) > 1:
        raw_data = raw_data[raw_data['State Code']==raw_data['State']]

    #raw_data.to_csv("TestRaw.csv")

    # Use state avg if we have no data at all for a particular indicator
    state = raw_data['State Code'].iloc[0]
    state_ind = data[data['State Code']==state]

    state_ind = state_ind.groupby(['State_x']).mean()

    # Parse in data
    for j in known_time_range:
        city_data['Price'].iloc[j-2000] = raw_data[str(j)]
    
    # Fill in missing values
    first = 0
    last = city_data.shape[0] - 1

    # Add constant values on start and end regions
    while np.isnan(city_data['Price'].iloc[first]):
        first += 1
    city_data['Price'].iloc[0:first] = city_data['Price'].iloc[first]

    while np.isnan(city_data['Price'].iloc[last]):
        last -= 1
    city_data['Price'].iloc[last:] = city_data['Price'].iloc[last-1]

    # Parse in indicator data
    population = [j for j in data.columns if 'POPESTIMATE' in j]
    for j in population:
        city_data['Population'].iloc[int(j[-4:])-2000] = raw_data[j]
    if city_data[['Year', 'Population']].dropna(subset=['Population']).shape[0] == 0:
        for j in population:
            city_data['Population'].iloc[int(j[-4:])-2000] = state_ind[0][j]

    unemp = [j for j in data.columns if 'Ue' in j]
    for j in unemp:
        city_data['Unemployment'].iloc[int(j[-4:])-2000] = raw_data[j]
    if city_data[['Year', 'Unemployment']].dropna(subset=['Unemployment']).shape[0] == 0:
        for j in unemp:
            city_data['Unemployment'].iloc[int(j[-4:])-2000] = state_ind.iloc[0][j]

    gdp = [j for j in data.columns if 'GDP' in j]
    for j in gdp:
        city_data['GDP'].iloc[int(j[-4:])-2000] = raw_data[j]
    if city_data[['Year', 'GDP']].dropna(subset=['GDP']).shape[0] == 0:
        for j in gdp:
            city_data['GDP'].iloc[int(j[-4:])-2000] = state_ind.iloc[0][j]

    b_perm = [j for j in data.columns if 'BP' in j]
    for j in b_perm:
        city_data['Building Permits'].iloc[int(j[-4:])-2000] = raw_data[j]
    if city_data[['Year', 'Building Permits']].dropna(subset=['Building Permits']).shape[0] == 0:
        for j in b_perm:
            city_data['Building Permits'].iloc[int(j[-4:])-2000] = state_ind.iloc[0][j]

    # Interest rate is US wide
    for j in range(len(intRate.iloc[0])):
        city_data['Interest rate'].iloc[j] = intRate.iloc[0,j]

    # Fill in missing values
    # Use simple linear regression to get missing data on endpoints
    for j in range(len(indicators)):
        loc = city_data[['Year', indicators[j]]].dropna(subset=[indicators[j]])

        if loc.shape[0] == 0:
            # Remove missing indicators (sometimes missing in whole state)
            local_indicators.remove(indicators[j])
        else:
            x = loc.iloc[:, 0].values.reshape(-1, 1)
            y = loc.iloc[:, 1].values.reshape(-1, 1)

            # Use OLS regression to fill in missing indicator values
            x_ols = sm.add_constant(x)
            res = sm.OLS(y, x_ols).fit()
        
            for k in range(city_data.shape[0]):
                if np.isnan(city_data[indicators[j]].iloc[k]):
                    pred_ols = res.get_prediction([1,k+2000])
                    city_data[indicators[j]].iloc[k] = pred_ols.summary_frame(alpha=CI_LOWER)['mean'][0]

    # Add user specified values
    for i in range(len(variables)):
        last_known = city_data[variables[i]].iloc[end_known-2000]
        # Linearly interpolate that indicator to get a smooth trend
        city_data[variables[i]].iloc[start_pred-2000:] = np.interp(x = list(prediction_time_range), xp = [end_known, end_pred], fp = [last_known, float(vals[i])])

    # Add new house prices
    known_data = city_data.iloc[:start_pred-2000,:]
    forecast_data = city_data.iloc[start_pred-2000:,:]

    #forecast_data[local_indicators].to_csv("test_output.csv")

    # Forecast future house prices
    forecast_res = loaded_model.get_forecast(steps=end_pred-end_known, step=1, exog=forecast_data[local_indicators])
    forecast_mean = round(forecast_res.predicted_mean)
    
    # Get confidence intervals
    forecast_min = round(forecast_res.conf_int(0.05)['lower Price'])    
    forecast_max = round(forecast_res.conf_int(0.05)['upper Price'])
    for j in range(23, len(forecast_min)+23):
        if forecast_min[j] < 1:
            forecast_min[j] = 1
        if forecast_mean[j] < 1:
            forecast_mean[j] = 1
        if forecast_max[j] < 1:
            forecast_max[j] = 1

    house_prices.iloc[0,:start_pred-2000] = round(known_data['Price'])
    house_prices.iloc[0,start_pred-2000:] = forecast_mean

    house_prices.iloc[1,:start_pred-2000] = round(known_data['Price'])
    house_prices.iloc[1,start_pred-2000:] = forecast_min
    house_prices.iloc[2,:start_pred-2000] = round(known_data['Price'])
    house_prices.iloc[2,start_pred-2000:] = forecast_max

    name = ""
    old_targ_indicators = {}
    old_targ_indicators['city'] = ''
    old_targ_indicators['year'] = ''
    old_targ_indicators['pop'] = ''
    old_targ_indicators['unemp'] = ''
    old_targ_indicators['gdp'] = ''
    old_targ_indicators['b_perm'] = ''
    old_targ_indicators['int'] = ''

    if os.path.isfile("Dynamic_price_prediction.csv"):
        old_data = pd.read_csv(f"Dynamic_price_prediction.csv", index_col="City")
        old_data = old_data.iloc[:3,]
        name = old_data.index[0]
        #print("Concat")
        #print(house_prices.columns[-1])
        #print(old_data.columns[-1])
        #print(house_prices)
        #print(old_data)

        # Check if it is not the exact same request
        f_name = 'city_data/' + name + '_dynamic.csv'
        old_full_data = pd.read_csv(f_name, index_col=0)
        print("old full")
        print(old_full_data.head())

        # Must be same city & year
        if (name == city) and (house_prices.size == old_data.size):
            print("Same city & year")
            old_data.index = [city, "Price_min", "Price_max"]

            # Has to have different inputs though
            if (~math.isnan(old_full_data['Population'].iloc[-1])) & (~math.isnan(city_data['Population'].iloc[-1])) & (old_full_data['Population'].iloc[-1] != city_data['Population'].iloc[-1]) | \
                (~math.isnan(old_full_data['Unemployment'].iloc[-1])) & (~math.isnan(city_data['Unemployment'].iloc[-1])) & (old_full_data['Unemployment'].iloc[-1] != city_data['Unemployment'].iloc[-1]) | \
                (~math.isnan(old_full_data['GDP'].iloc[-1])) & (~math.isnan(city_data['GDP'].iloc[-1])) & (old_full_data['GDP'].iloc[-1] != city_data['GDP'].iloc[-1]) | \
                (~math.isnan(old_full_data['Building Permits'].iloc[-1])) & (~math.isnan(city_data['Building Permits'].iloc[-1])) & (old_full_data['Building Permits'].iloc[-1] != city_data['Building Permits'].iloc[-1]) | \
                (~math.isnan(old_full_data['Interest rate'].iloc[-1])) & (~math.isnan(city_data['Interest rate'].iloc[-1])) & (old_full_data['Interest rate'].iloc[-1] != city_data['Interest rate'].iloc[-1]):

                print("Counterfacutual pred")
                house_prices = pd.concat([house_prices, old_data.iloc[:3]])

                # Add previous prediction (if relevant)
                old_targ_indicators['city'] = city
                old_targ_indicators['year'] = str(year)
                old_targ_indicators['pop'] = str(round(int(old_full_data['Population'].iloc[-1])))
                old_targ_indicators['unemp'] = str(old_full_data['Unemployment'].iloc[-1])
                old_targ_indicators['gdp'] = str(round(int(old_full_data['GDP'].iloc[-1])))
                old_targ_indicators['b_perm'] = str(round(int(old_full_data['Building Permits'].iloc[-1])))
                old_targ_indicators['int'] = str(old_full_data['Interest rate'].iloc[-1])

            else: # Do nothing
                print("Same prediction")
                return {}

    # Save dynamic predictions
    house_prices.index.name='City'
    house_prices.to_csv('Dynamic_price_prediction.csv', index=True)

    # Save city information
    city_data['Price'].iloc[start_pred-2000:] = forecast_mean
    city_data['Price min'].iloc[start_pred-2000:] = forecast_min
    city_data['Price max'].iloc[start_pred-2000:] = forecast_max
    city_data['Price min'].iloc[:start_pred-2000] = city_data['Price'].iloc[:start_pred-2000]
    city_data['Price max'].iloc[:start_pred-2000] = city_data['Price'].iloc[:start_pred-2000]
    city_data.to_csv('city_data/' + city + '_dynamic' + '.csv', index=True)

    # Return target year indicators (if applicable)
    return old_targ_indicators

if __name__ == "__main__":
    predict(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
