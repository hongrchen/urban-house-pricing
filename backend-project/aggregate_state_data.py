# Aggragate data of cities to state level and store to state files

import pandas as pd
import numpy as np
import os
from os.path import join

city_state = pd.read_csv(f"backend-project/data/merged_data_extended.csv")
states = city_state['STNAME'].unique()
state_cities = {}
for state in states:
    state_cities[state] = []

cities = []
for file in os.listdir('backend-project/city_data'):
    cities.append(file[:-4])

for i in range(city_state.shape[0]):
    if city_state['NAME'].iloc[i] in cities:
        state_cities[city_state['STNAME'].iloc[i]] = state_cities[city_state['STNAME'].iloc[i]]+[city_state['NAME'].iloc[i]]

# AGGREGATION METHOD
# Year: 1st
# Price: population weighted avg
# State: 1st
# Region: 1st
# Population: sum
# Unemployment: population weighted avg
# GDP: population weighted avg
# Building Permits: sum
# Interest rate: 1st (should be same anyway)

columns = ['Year', 'Price', 'State', 'Region', 'Population', 'Unemployment', 'GDP', 'Building Permits', 'Interest rate']

for state in states:
    state_data = pd.DataFrame(np.NAN, index = range(41), columns=columns)
    state_data['Year'] = range(2000, 2041)

    cities = state_cities[state]
    raw_data = []
    for city in cities:
        raw_data.append(pd.read_csv(f"backend-project/city_data/"+city+".csv"))

    # For these indicators, just use the value of the 1st one
    state_data['State'] = raw_data[0]['State']
    state_data['Region'] = raw_data[0]['Region']
    state_data['Interest rate'] = raw_data[0]['Interest rate']

    state_data['Price'] = 0
    state_data['Population'] = 0
    state_data['Unemployment'] = 0
    state_data['GDP'] = 0
    state_data['Building Permits'] = 0

    for i in range(len(raw_data)):
        state_data['Price'] += raw_data[i]['Price']
        state_data['Population'] += raw_data[i]['Population']
        state_data['Unemployment'] += raw_data[i]['Unemployment']
        state_data['GDP'] += raw_data[i]['GDP']
        state_data['Building Permits'] += raw_data[i]['Building Permits']
    
    state_data['Price'] = round(state_data['Price']/len(raw_data))
    state_data['Unemployment'] = round(state_data['Unemployment']/len(raw_data), 1)
    state_data['GDP'] = round(state_data['Price']/len(raw_data))
    
    state_data.to_csv('backend-project/state_data/' + state + '.csv', index=True)

print("Done")