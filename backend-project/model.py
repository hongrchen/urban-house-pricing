from calendar import c
import numpy as np
import pandas as pd
from pandas.core.common import SettingWithCopyWarning
from sklearn import linear_model
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import GradientBoostingRegressor
from mlinsights.mlmodel import IntervalRegressor
from sklearn.metrics import mean_squared_error, r2_score
from statsmodels.tsa.arima.model import ARIMA, ARIMAResults
from statsmodels.tsa.statespace.sarimax import SARIMAX
import statsmodels.api as sm
from itertools import chain
from copy import copy
import warnings
warnings.filterwarnings("ignore")
warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)

# Load data
data = pd.read_csv('backend-project/data/merged_data_extended.csv', index_col='City')
state_region = pd.read_csv('backend-project/data/State_Region.csv')
gdp = pd.read_csv('backend-project/data/GDP_US_byState_annualy_2005_2021.csv', sep=';')
gdp.columns = list(gdp.columns[:1]) + ['GDP'+i for i in gdp.columns[1:]]

state_region = state_region.merge(gdp, how='left', on='State')
data = data.reset_index().merge(state_region, how='left', left_on='STNAME', right_on='State').set_index(data.index)

bp = pd.read_csv('backend-project/data/BuildingPermits_US_merged_2004_to_2019.csv', sep=',')
bp = bp.iloc[:,[2]+list(range(4,bp.shape[1]))]
bp.columns = list(bp.columns[:1]) + ['BP'+i for i in bp.columns[1:]]
data = data.merge(bp, how='left', left_on='NAME', right_on='Name')

data.drop_duplicates(subset=['City'], keep='first', inplace=True)

intRate = pd.read_csv('backend-project/data/InterestRate_2000_2021.csv', sep=',')
intRate.drop(labels=['Indicator'], axis=1, inplace=True)

start_known = 2000
end_known = 2022
start_pred = 2023
end_pred = 2040
known_time_range = range(start_known, end_known+1)
prediction_time_range = range(start_pred, end_pred+1)

indicators = ['Population', 'Unemployment', 'GDP', 'Building Permits', 'Interest rate']
columns = ['Year', 'Price', 'Price min', 'Price max', 'State', 'Region'] + indicators + [i+' min' for i in indicators] + [i+' max' for i in indicators]

cities = data.index
years = [str(i+2000) for i in range(end_pred+1 - start_known)]
house_prices = pd.DataFrame(np.NAN, index = cities, columns=years)
house_prices_min = pd.DataFrame(np.NAN, index = cities, columns=years)
house_prices_max = pd.DataFrame(np.NAN, index = cities, columns=years)
house_prices_change = pd.DataFrame(np.NAN, index = cities, columns=years)

# Set quantiles for confidence intervals
CI_LOWER = .05
CI_UPPER = 1 - CI_LOWER

# Iterate over cities
for i in range(data.shape[0]):

    local_indicators = copy(indicators)

    city = cities[i]
    if city % 10 == 0:
        print(city)
    
    city_data = pd.DataFrame(np.NAN, index = range(end_pred+1 - start_known), columns=columns)
    city_data['Year'] = range(start_known, end_pred+1)
    raw_data = data.iloc[i,:]
    city_data['State'] = (end_pred+1 - start_known) * [raw_data['STNAME']]
    city_data['Region'] = (end_pred+1 - start_known) * [raw_data['Region']]

    # Use state avg if we have no data at all for a particular indicator
    state = data['State Code'].iloc[i]
    state_ind = data[data['State Code']==state]

    state_ind = state_ind.groupby(['State_x']).mean()

    # Parse in data
    for j in known_time_range:
        city_data['Price'].iloc[j-2000] = raw_data[str(j)]

    population = [j for j in data.columns if 'POPESTIMATE' in j]
    for j in population:
        city_data['Population'].iloc[int(j[-4:])-2000] = raw_data[j]
    if city_data[['Year', 'Population']].dropna(subset=['Population']).shape[0] == 0:
        for j in population:
            city_data['Population'].iloc[int(j[-4:])-2000] = state_ind[0][j]

    unemp = [j for j in data.columns if 'Ue' in j]
    for j in unemp:
        city_data['Unemployment'].iloc[int(j[-4:])-2000] = raw_data[j]
    if city_data[['Year', 'Unemployment']].dropna(subset=['Unemployment']).shape[0] == 0:
        for j in unemp:
            city_data['Unemployment'].iloc[int(j[-4:])-2000] = state_ind.iloc[0][j]

    gdp = [j for j in data.columns if 'GDP' in j]
    for j in gdp:
        city_data['GDP'].iloc[int(j[-4:])-2000] = raw_data[j]
    if city_data[['Year', 'GDP']].dropna(subset=['GDP']).shape[0] == 0:
        for j in gdp:
            city_data['GDP'].iloc[int(j[-4:])-2000] = state_ind.iloc[0][j]

    b_perm = [j for j in data.columns if 'BP' in j]
    for j in b_perm:
        city_data['Building Permits'].iloc[int(j[-4:])-2000] = raw_data[j]
    if city_data[['Year', 'Building Permits']].dropna(subset=['Building Permits']).shape[0] == 0:
        for j in b_perm:
            city_data['Building Permits'].iloc[int(j[-4:])-2000] = state_ind.iloc[0][j]

    # Interest rate is US wide
    for j in range(len(intRate.iloc[0])):
        city_data['Interest rate'].iloc[j] = intRate.iloc[0,j]
        
    # Collect starting year for all indicators
    starts = [0, 0, 0, 0, 0]

    # Fill in missing values
    # Use simple linear regression to get missing data on endpoints
    for j in range(len(indicators)):
        loc = city_data[['Year', indicators[j]]].dropna(subset=[indicators[j]])

        if loc.shape[0] == 0:
            # Remove missing indicators (sometimes missing in whole state)
            local_indicators.remove(indicators[j])
        else:
            x = loc.iloc[:, 0].values.reshape(-1, 1)
            y = loc.iloc[:, 1].values.reshape(-1, 1)

            starts[j] = x[0] - 2000

            # Use OLS regression to fill in missing indicator values
            x_ols = sm.add_constant(x)
            res = sm.OLS(y, x_ols).fit()
            
            # Predict future indicator values
            for k in range(city_data.shape[0]):
                if np.isnan(city_data[indicators[j]].iloc[k]):
                    pred_ols = res.get_prediction([1,k+2000])
                    if indicators[j] == 'Interest rate':
                        city_data[indicators[j]].iloc[k] = round(pred_ols.summary_frame(alpha=CI_LOWER)['mean'][0], 1)
                        city_data[indicators[j]+' min'].iloc[k] = round(pred_ols.summary_frame(alpha=CI_LOWER)['obs_ci_lower'][0], 1)
                        city_data[indicators[j]+' max'].iloc[k] = round(pred_ols.summary_frame(alpha=CI_LOWER)['obs_ci_upper'][0], 1)
                    # Population, building permits, GDP, Unemployment shouldn't be negative
                    elif indicators[j] == 'Unemployment': # round to 1 d.p.
                        city_data[indicators[j]].iloc[k] = max(0, round(pred_ols.summary_frame(alpha=CI_LOWER)['mean'][0], 1))
                        city_data[indicators[j]+' min'].iloc[k] = max(0, round(pred_ols.summary_frame(alpha=CI_LOWER)['obs_ci_lower'][0], 1))
                        city_data[indicators[j]+' max'].iloc[k] = max(0, round(pred_ols.summary_frame(alpha=CI_LOWER)['obs_ci_upper'][0], 1))
                    else:# round to whole num
                        city_data[indicators[j]].iloc[k] = max(0, round(pred_ols.summary_frame(alpha=CI_LOWER)['mean'][0]))
                        city_data[indicators[j]+' min'].iloc[k] = max(0, round(pred_ols.summary_frame(alpha=CI_LOWER)['obs_ci_lower'][0]))
                        city_data[indicators[j]+' max'].iloc[k] = max(0, round(pred_ols.summary_frame(alpha=CI_LOWER)['obs_ci_upper'][0]))
                else:
                    # If the value is known, then the lower/upper bounds are simply set to that value (no uncertainty)
                    if indicators[j] == 'Interest rate' or indicators[j] == 'Unemployment':
                        city_data[indicators[j]+' min'].iloc[k] = round(city_data[indicators[j]].iloc[k], 1)
                        city_data[indicators[j]+' max'].iloc[k] = round(city_data[indicators[j]].iloc[k], 1)
                        city_data[indicators[j]].iloc[k] = round(city_data[indicators[j]].iloc[k], 1)
                    else:# round to whole num
                        city_data[indicators[j]+' min'].iloc[k] = round(city_data[indicators[j]].iloc[k])
                        city_data[indicators[j]+' max'].iloc[k] = round(city_data[indicators[j]].iloc[k])
                        city_data[indicators[j]].iloc[k] = round(city_data[indicators[j]].iloc[k])

    # Add missing prices just for known past (we want to predict them for year 2023+ using ARIMA)
    known_prices = city_data[['Year', 'Price']].dropna(subset=['Price'])

    x = known_prices.iloc[:, 0].values.reshape(-1, 1)
    y = known_prices.iloc[:, 1].values.reshape(-1, 1)

    # Use OLS regression to fill in missing price values
    x_ols = sm.add_constant(x)
    res = sm.OLS(y, x_ols).fit()

    for k in range(end_known - start_known):
        if np.isnan(city_data['Price'].iloc[k]):
            pred_ols = res.get_prediction([1,k+2000])
            city_data['Price'].iloc[k] = max(1, round(pred_ols.summary_frame(alpha=CI_LOWER)['mean'][0]))

    # Create a SARIMAX model
    known_data = city_data.iloc[:start_pred-2000,:]
    forecast_data = city_data.iloc[start_pred-2000:,:]
    model = SARIMAX(endog=known_data[['Price']], exog=known_data[local_indicators], order=(len(local_indicators),1,0), use_exact_diffuse=True)
    model_fit = model.fit(disp=False)

    # Forecast future house prices
    forecast_res = model_fit.get_forecast(steps=end_pred-end_known, step=1, exog=forecast_data[local_indicators])
    forecast_mean = round(forecast_res.predicted_mean)
    
    # Get confidence intervals
    forecast_min = round(forecast_res.conf_int(CI_LOWER)['lower Price'])    
    forecast_max = round(forecast_res.conf_int(CI_LOWER)['upper Price'])
    for j in range(23, len(forecast_min)+23):
        if forecast_min[j] < 1:
            forecast_min[j] = 1
        if forecast_mean[j] < 1:
            forecast_mean[j] = 1
        if forecast_max[j] < 1:
            forecast_max[j] = 1

    house_prices.iloc[i,:start_pred-2000] = known_data['Price']
    house_prices.iloc[i,start_pred-2000:] = forecast_mean

    house_prices_min.iloc[i,:start_pred-2000] = known_data['Price']
    house_prices_max.iloc[i,:start_pred-2000] = known_data['Price']
    house_prices_min.iloc[i,start_pred-2000:] = forecast_min
    house_prices_max.iloc[i,start_pred-2000:] = forecast_max

    # Calculate the house prices as a percentage compaered to 2022 & round them all
    for j in range(end_pred-start_known+1):
        house_prices_change.iloc[i,j] = round(house_prices.iloc[i,j] / house_prices.iloc[i,21] * 100)
        house_prices.iloc[i,j] = round(house_prices.iloc[i,j])

    # Save model
    model_fit.save('backend-project/city_models/' + data['City'].iloc[i] + '.pkl')

    # Save city information
    city_data['Price'] = np.round(city_data['Price'])
    city_data['Price'].iloc[start_pred-2000:] = forecast_mean
    city_data['Price min'].iloc[start_pred-2000:] = forecast_min
    city_data['Price max'].iloc[start_pred-2000:] = forecast_max
    city_data['Price min'].iloc[:start_pred-2000] = city_data['Price'].iloc[:start_pred-2000]
    city_data['Price max'].iloc[:start_pred-2000] = city_data['Price'].iloc[:start_pred-2000]

    # Remove predicted past indicator confidence intervals (to not display them on the dashboard)
    for j in range(len(indicators)):
        if indicators[j] in local_indicators:
            city_data[indicators[j]+' min'].iloc[:starts[j][0]] = city_data[indicators[j]].iloc[:starts[j][0]]
            city_data[indicators[j]+' max'].iloc[:starts[j][0]] = city_data[indicators[j]].iloc[:starts[j][0]]
    
    city_data.to_csv('backend-project/city_data/' + data['City'].iloc[i] + '.csv', index=True)

# Save static predictions
house_prices.index = data['City']
house_prices.to_csv('backend-project/Static_price_prediction.csv', index=True)

house_prices_min.index = data['City']
house_prices_max.index = data['City']
house_prices_change.index = data['City']
house_prices_min.to_csv('backend-project/Static_price_prediction_min.csv', index=True)
house_prices_max.to_csv('backend-project/Static_price_prediction_max.csv', index=True)
house_prices_change.to_csv('backend-project/Static_price_prediction_change.csv', index=True)
