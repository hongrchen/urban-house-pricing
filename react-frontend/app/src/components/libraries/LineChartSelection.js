import React from "react";
import { Line } from "react-chartjs-2";
import { Chart as ChartJS } from "chart.js/auto";

function LineChartSelection({chartData,setpredictionCity,setpredictionYear,RadioSelect,chart_title}) {
    return <Line data={chartData} 
    options={{
      animation: {
        duration: 500
    },
      scales: { yAxes: {title: {
        display: true,
        text: function(){ if (!RadioSelect){return "Price ($)"}else{return "Price w.r.t. 2022 (%)"}},
        font: {
          size: 12
          }
          }
          },
          xAxes: {title: {
              display: true,
              text: "Year",
              font: {
                  size: 12
              }
            }
          },  
       },

      plugins: {
        title: {
        display: true,
        text: chart_title
          },
      legend: {
        onClick: function (e, legendItem) {   
          setpredictionCity(legendItem.text)
          setpredictionYear(2030)
          return null
      },
        display: true,
        labels: {
            filter: function (legendItem, chartData) {
              return (chartData.datasets[legendItem.datasetIndex].label)
            },
        }
    },
    tooltip: {
      filter: function (tooltipItem, data) {
        var label = chartData.datasets[tooltipItem.datasetIndex].label;
        if ((label == null) ) {
          return false;
        } else {
          return true;
        }
   }}
  },
        onClick: (event, elements) => {

          var datasetIndex=elements[0].index;
          var year_selected = chartData.labels[datasetIndex];
          var city_selected=chartData.datasets[elements[0].datasetIndex].city
          setpredictionCity(city_selected)
          setpredictionYear(year_selected)

        },

      } }
    />;
}

export default LineChartSelection;




