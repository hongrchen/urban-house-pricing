import React, { useState, useEffect } from "react";
import { scaleQuantize } from "d3-scale";
import {scaleLinear } from "d3";
import queryBackend from './backend/BackendQueryEngine';
import styled from "styled-components";
import colorLib from '@kurkle/color';
import LineChartSelection from "./components/libraries/LineChartSelection";
import LineChartSelectionProp from "./components/libraries/LineChartSelectionProp";
import LineChart from "./components/libraries/LineChart";
import BarChart from "./components/libraries/BarChart";
import icon2 from './icon2.svg';

const ChartsComponent = ({state1,state2,city1,city2,setpredictionCity,setpredictionYear,setglobalProperty}) => {
  
// Visualization style
function transparentize(value, opacity) {
  var alpha = opacity === undefined ? 0.5 : 1 - opacity;
  return colorLib(value).alpha(alpha).rgbString();
}
const CHART_COLORS = {
  red: 'rgb(255, 99, 132)',
  orange: 'rgb(255, 159, 64)',
  yellow: 'rgb(255, 205, 86)',
  green: 'rgb(75, 192, 192)',
  blue: 'rgb(54, 162, 235)',
  purple: 'rgb(153, 102, 255)',
  grey: 'rgb(201, 203, 207)'
};
// Specify datasets for drawing chart plots

const Main = styled("div")`
font-family: sans-serif;
background: #f0f0f0;
height: 10vh;
`;

const DropDownContainer = styled("div")`
width: 15.5em;
background: #f0f0f0;
margin: 0 auto;
`;

const DropDownHeader = styled("div")`
margin: 0.5em;
padding: 0.4em 2em 0.4em 1em;
box-shadow: 0 2px 3px rgba(0, 0, 0, 0.15);
font-weight: 500;
font-size: 1.0rem;
color: #3faffa;
background: #f0f0f0;
`;

const DropDownListContainer = styled("div")`
overflow: scroll;
height: 300px;
`;

const DropDownList = styled("ul")`
padding: 0;
margin: 0;
padding-left: 1em;
background: #ffffff;
border: 2px solid #e5e5e5;
box-sizing: border-box;
color: #3faffa;
font-size: 0.8rem;
font-weight: 500;
&:first-child {
  padding-top: 0.5em;
}
`;

const ListItem = styled("li")`
list-style: none;
margin-bottom: 0.8em;
`;

const [prices, setPrices] = useState([]);
useEffect(()=>{
  queryBackend(`get-prices`).then((prices) => {
    setPrices(prices);
});
}, [])

// Options' list for drop-down buttons 
const options = [...new Set(prices.map(item => item.City))];


// Create dropdown buttons
const [isOpen1,  setIsOpen1] = useState(false);
const [selectedOption1, setSelectedOption1] = useState(false);

const [isOpen2, setIsOpen2] = useState(false);
const [selectedOption2, setSelectedOption2] = useState(false);

const toggling1 = () => setIsOpen1(!isOpen1);
const toggling2 = () => setIsOpen2(!isOpen2);

// useEffect(() => {});

const onOptionClicked1 = value => () => {
setSelectedOption1(value);
setIsOpen1(false);
console.log(selectedOption1);

};
const onOptionClicked2 = value => () => {
setSelectedOption2(value);
setIsOpen2(false);
console.log(selectedOption2);
};

const [variables, setVariables] = useState("");
const [RadioSelect, setRadioSelect] = useState(false);
const setRadio = () => {
  if (RadioSelect==false){setVariables("_change");}else{setVariables("");}
  setRadioSelect(!RadioSelect)
};

// Backend query to query the city data for the property line chart
const [property, setProperty] = useState("Population");
const [sel_city, setCity] = useState("Dallas");
const [dataPropC1, setDataPropC1] = useState([]);
const [dataPropC2, setDataPropC2] = useState([]);

// first city
useEffect(() => {
  queryBackend(`get-properties?pro=`+property+`&city=`+selectedOption1).then((states) => {
      setDataPropC1(states);
  });

}, [property, selectedOption1]);

// second city
useEffect(() => {
  queryBackend(`get-properties?pro=`+property+`&city=`+selectedOption2).then((states) => {
      setDataPropC2(states);
  });

}, [property, selectedOption2]);



// for price plot

const [userDataState, setUserDataState] = useState({
  labels: prices.filter((a) => a.City === selectedOption1??"Dallas" ).map((data) => data.year),
  datasets: [
    {
        label: selectedOption1??"Dallas",
        city: selectedOption1??"Dallas",
        data: prices.filter((a) => a.City === selectedOption1??"Dallas" ).map((data) => data['price'+variables]),
        fill: false, // use "True" to draw area-plot 
        borderColor: CHART_COLORS.red,
        backgroundColor: transparentize(CHART_COLORS.red, 0.8),
        pointBackgroundColor: 'black',
        pointBorderColor:'black'
    },
    {
      
      city: selectedOption1??"Dallas",
      data: prices.filter((a) => a.City === selectedOption1??"Dallas" ).map((data) => data['price_min'+variables]),
      fill: 0, // use "True" to draw area-plot 
      borderColor: CHART_COLORS.red,
      backgroundColor: transparentize(CHART_COLORS.red, 0.8),
      pointBackgroundColor: 'transparent',
      pointBorderColor:'transparent'
  },
  {
    
    city: selectedOption1??"Dallas",
    data: prices.filter((a) => a.City === selectedOption1??"Dallas" ).map((data) => data['price_max'+variables]),
    fill: 0, // use "True" to draw area-plot 
    borderColor: CHART_COLORS.red,
    backgroundColor: transparentize(CHART_COLORS.red, 0.8),
    pointBackgroundColor: 'transparent',
    pointBorderColor:'transparent'
  },
    {
        label: selectedOption2??"Oakland",
        city: selectedOption2??"Oakland",
        data: prices.filter((a) => a.City === selectedOption2??"Oakland").map((data) => data['price'+variables]),
        fill: false,
        borderColor: CHART_COLORS.blue,
        backgroundColor: transparentize(CHART_COLORS.blue, 0.8),
        pointBackgroundColor: 'black',
        pointBorderColor:'black'
    },
    {
      
      city: selectedOption2??"Oakland",
      data: prices.filter((a) => a.City === selectedOption2??"Oakland" ).map((data) => data['price_min'+variables]),
      fill: 3, // use "True" to draw area-plot 
      borderColor: CHART_COLORS.blue,
      backgroundColor: transparentize(CHART_COLORS.blue, 0.8),
      pointBackgroundColor: 'transparent',
      pointBorderColor:'transparent',
      hiddenLegend: true,
  },
  {
    
    city: selectedOption2??"Oakland",
    data: prices.filter((a) => a.City === selectedOption2??"Oakland" ).map((data) => data['price_max'+variables]),
    fill: 3, // use "True" to draw area-plot 
    borderColor: CHART_COLORS.blue,
    backgroundColor: transparentize(CHART_COLORS.blue, 0.8),
    pointBackgroundColor: 'transparent',
    pointBorderColor:'transparent'
  }
  ]})


useEffect(() => {
  console.log(prices.filter((a) => a.City === "Dallas" )) 
setUserDataState({
      labels: prices.filter((a) => a.City === selectedOption1??"Dallas" ).map((data) => data.year),
      datasets: [
          {
              label: selectedOption1??"Dallas",
              city: selectedOption1??"Dallas",
              data: prices.filter((a) => a.City === selectedOption1??"Dallas" ).map((data) => data['price'+variables]),
              fill: false, // use "True" to draw area-plot 
              borderColor: CHART_COLORS.red,
              backgroundColor: transparentize(CHART_COLORS.red, 0.8),
              pointBackgroundColor: 'black',
              pointBorderColor:'black'
          },
          {
            
            city: selectedOption1??"Dallas",
            data: prices.filter((a) => a.City === selectedOption1??"Dallas" ).map((data) => data['price_min'+variables]),
            fill: 0, // use "True" to draw area-plot 
            borderColor: CHART_COLORS.red,
            backgroundColor: transparentize(CHART_COLORS.red, 0.8),
            pointBackgroundColor: 'transparent',
            pointBorderColor:'transparent'
        },
        {
          
          city: selectedOption1??"Dallas",
          data: prices.filter((a) => a.City === selectedOption1??"Dallas" ).map((data) => data['price_max'+variables]),
          fill: 0, // use "True" to draw area-plot 
          borderColor: CHART_COLORS.red,
          backgroundColor: transparentize(CHART_COLORS.red, 0.8),
          pointBackgroundColor: 'transparent',
          pointBorderColor:'transparent'
        },
          {
              label: selectedOption2??"Oakland",
              city: selectedOption2??"Oakland",
              data: prices.filter((a) => a.City === selectedOption2??"Oakland").map((data) => data['price'+variables]),
              fill: false,
              borderColor: CHART_COLORS.blue,
              backgroundColor: transparentize(CHART_COLORS.blue, 0.8),
              pointBackgroundColor: 'black',
              pointBorderColor:'black'
          },
          {
            
            city: selectedOption2??"Oakland",
            data: prices.filter((a) => a.City === selectedOption2??"Oakland" ).map((data) => data['price_min'+variables]),
            fill: 3, // use "True" to draw area-plot 
            borderColor: CHART_COLORS.blue,
            backgroundColor: transparentize(CHART_COLORS.blue, 0.8),
            pointBackgroundColor: 'transparent',
            pointBorderColor:'transparent',
            hiddenLegend: true,
        },
        {
          
          city: selectedOption2??"Oakland",
          data: prices.filter((a) => a.City === selectedOption2??"Oakland" ).map((data) => data['price_max'+variables]),
          fill: 3, // use "True" to draw area-plot 
          borderColor: CHART_COLORS.blue,
          backgroundColor: transparentize(CHART_COLORS.blue, 0.8),
          pointBackgroundColor: 'transparent',
          pointBorderColor:'transparent'
        }
        ],});

}, [selectedOption1,selectedOption2,RadioSelect]);


// for property plot

const [userDataStateProp, setUserDataStateProp] = useState({
  labels: dataPropC1.map((data) => data.Year),
  datasets: [
    {
        label: selectedOption1??"Dallas",
        city: selectedOption1??"Dallas",
        data: dataPropC1.map((data) => data[property]),
        fill: false, // use "True" to draw area-plot 
        borderColor: CHART_COLORS.red,
        backgroundColor: transparentize(CHART_COLORS.red, 0.5),
        pointBackgroundColor: 'black',
        pointBorderColor:'black'
    },
    {
      
      city: selectedOption1??"Dallas",
      data: dataPropC1.map((data) => data[property+' min']),
      fill: 0, // use "True" to draw area-plot 
      borderColor: CHART_COLORS.red,
      backgroundColor: transparentize(CHART_COLORS.red, 0.5),
      pointBackgroundColor: 'transparent',
      pointBorderColor:'transparent'
  },
  {
    
    city: selectedOption1??"Dallas",
    data: dataPropC1.map((data) => data[property+' max']),
    fill: 0, // use "True" to draw area-plot 
    borderColor: CHART_COLORS.red,
    backgroundColor: transparentize(CHART_COLORS.red, 0.5),
    pointBackgroundColor: 'transparent',
    pointBorderColor:'transparent'
  },
    {
        label: selectedOption2??"Oakland",
        city: selectedOption2??"Oakland",
        data: dataPropC1.map((data) => data[property]),
        fill: false,
        borderColor: CHART_COLORS.blue,
        backgroundColor: transparentize(CHART_COLORS.blue, 0.5),
        pointBackgroundColor: 'black',
        pointBorderColor:'black'
    },
    {
      
      city: selectedOption2??"Oakland",
      data: dataPropC1.map((data) => data[property+' min']),
      fill: 3, // use "True" to draw area-plot 
      borderColor: CHART_COLORS.blue,
      backgroundColor: transparentize(CHART_COLORS.blue, 0.5),
      pointBackgroundColor: 'transparent',
      pointBorderColor:'transparent',
      hiddenLegend: true,
  },
  {
    
    city: selectedOption2??"Oakland",
    data: dataPropC1.map((data) => data[property+' max']),
    fill: 3, // use "True" to draw area-plot 
    borderColor: CHART_COLORS.blue,
    backgroundColor: transparentize(CHART_COLORS.blue, 0.5),
    pointBackgroundColor: 'transparent',
    pointBorderColor:'transparent'
  }
  ]})


useEffect(() => { 
  // console.log(dataProp)
  // console.log(dataProp.Population)
  // console.log(dataProp.map((data) => data.Population))
  // console.log(dataProp.map((data) => data[property]))
  setUserDataStateProp({
        labels: dataPropC1.map((data) => data.Year),
        datasets: [
            {
                label: selectedOption1??"Dallas",
                city: selectedOption1??"Dallas",
                data: dataPropC1.map((data) => data[property]),
                fill: false, // use "True" to draw area-plot 
                borderColor: CHART_COLORS.red,
                backgroundColor: transparentize(CHART_COLORS.red, 0.5),
                pointBackgroundColor: 'black',
                pointBorderColor:'black'
            },
            {
              
              city: selectedOption1??"Dallas",
              data: dataPropC1.map((data) => data[property+' min']),
              fill: 0, // use "True" to draw area-plot 
              borderColor: CHART_COLORS.red,
              backgroundColor: transparentize(CHART_COLORS.red, 0.5),
              pointBackgroundColor: 'transparent',
              pointBorderColor:'transparent'
          },
          {
            
            city: selectedOption1??"Dallas",
            data: dataPropC1.map((data) => data[property+' max']),
            fill: 0, // use "True" to draw area-plot 
            borderColor: CHART_COLORS.red,
            backgroundColor: transparentize(CHART_COLORS.red, 0.5),
            pointBackgroundColor: 'transparent',
            pointBorderColor:'transparent'
          },
            {
                label: selectedOption2??"Oakland",
                city: selectedOption2??"Oakland",
                data: dataPropC2.map((data) => data[property]),
                fill: false,
                borderColor: CHART_COLORS.blue,
                backgroundColor: transparentize(CHART_COLORS.blue, 0.5),
                pointBackgroundColor: 'black',
                pointBorderColor:'black'
            },
            {
              
              city: selectedOption2??"Oakland",
              data: dataPropC2.map((data) => data[property+' min']),
              fill: 3, // use "True" to draw area-plot 
              borderColor: CHART_COLORS.blue,
              backgroundColor: transparentize(CHART_COLORS.blue, 0.5),
              pointBackgroundColor: 'transparent',
              pointBorderColor:'transparent',
              hiddenLegend: true,
          },
          {
            
            city: selectedOption2??"Oakland",
            data: dataPropC2.map((data) => data[property+' max']),
            fill: 3, // use "True" to draw area-plot 
            borderColor: CHART_COLORS.blue,
            backgroundColor: transparentize(CHART_COLORS.blue, 0.5),
            pointBackgroundColor: 'transparent',
            pointBorderColor:'transparent'
          }
          ],});
  
  }, [selectedOption1,selectedOption2,property,dataPropC1,dataPropC2]);


useEffect(() => { 
  setSelectedOption1(city1);
  }, [city1]);

  useEffect(() => { 
    setSelectedOption2(city2);
    }, [city2]);


const ChildDiv = styled("div")`
   {
    display: inline-block;
    vertical-align: text-top;
    margin: 10 auto;
    gap: 50rem;
  }
`;
  return (
    <>
    <div >
        <div>
          <h2>
            COMPARE <img src={icon2} width={35} height={35} alt="logo"/>
          <br/>
            House prices and socio-economic indicators
          </h2>
          <div style={{ display: "flex", gap: "0.5rem"}}>
          <ChildDiv style={{width:'50vh',
         position: 'absolute', left: '20%', top: '145%',
         transform: 'translate(-50%, -50%)'}}> 
            <Main><h3>Please choose two cities to compare:</h3>
              <div style={{ display: "flex", gap: "0.5rem"}}>
                <DropDownContainer>
                 
                <DropDownHeader onClick={toggling1}>
                {selectedOption1 || "Oakland"}
                </DropDownHeader>
                  {isOpen1 && (
                  <DropDownListContainer className="selection-area">
                  < DropDownList>
                     {options.map(option => (
                        <ListItem onClick={onOptionClicked1(option)} key={Math.random()}>
                            {option}
                        </ListItem>
                      ))}
                  </DropDownList>
                  </DropDownListContainer>
                )}
                </DropDownContainer>
            
              <DropDownContainer>
          
                <DropDownHeader onClick={toggling2}>
                {selectedOption2 || "Dallas"}
              </DropDownHeader>
              {isOpen2 && (
                <DropDownListContainer>
                  <DropDownList>
                     {options.map(option => (
                        <ListItem onClick={onOptionClicked2(option)} key={Math.random()}>
                            {option}
                        </ListItem>
                      ))}
                  </DropDownList>
                </DropDownListContainer>
                )}
              </DropDownContainer>
              </div>
            </Main>
          </ChildDiv>


          <ChildDiv style={{width:'70vh',
         position: 'absolute', left: '70%', top: '270%',
         transform: 'translate(-50%, -50%)'}}> 
            <div >
              <input
                type='checkbox'
                id='bar'
                name='chart'
                checked={RadioSelect}
                onClick={setRadio}
              />
              <label htmlFor="bar">% Price w.r.t. 2022</label>
            </div>
            <LineChartSelection chartData={userDataState} setpredictionCity={setpredictionCity} setpredictionYear={setpredictionYear} RadioSelect={RadioSelect} chart_title='Static House Price Comparison'
            />
            
            </ChildDiv> 

            <ChildDiv style={{width:'70vh',
         position: 'absolute', left: '20%', top: '535%',
         transform: 'translate(-50%, -50%)'}}>

            <h3>Select a socio-economic indicator:</h3>
            <select
            value={property}
            onChange={event => setProperty(event.target.value)}>
            <option value="Population">Population (count)</option>
            <option value="Unemployment">Unemployment (%)</option>
            <option value="GDP">Annually GDP (million $)</option>
            <option value="Building Permits">Building Permits (count)</option>
            <option value="Interest rate">Interest Rate (%)</option>
            </select>
            </ChildDiv>

            <ChildDiv style={{width:'70vh',
         position: 'absolute', left: '70%', top: '610%',
         transform: 'translate(-50%, -50%)'}}>

            {/* <LineChart chartData={userDataStateProp} 
            /> */}
            <LineChartSelectionProp chartData={userDataStateProp} setpredictionCity={setpredictionCity} setpredictionYear={setpredictionYear} RadioSelect={RadioSelect} chart_title='Static Indicator Comparison' yAxis={property}
            />

          </ChildDiv>
        </div>
        </div>
      </div>
    </>
  );
};

export default ChartsComponent;
