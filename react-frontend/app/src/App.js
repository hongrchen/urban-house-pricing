import React, { useState, useEffect } from 'react';
import './App.css';
import ChartsComponent from './ChartsComponent';
import GeoWrapper from './GeoWrapper';
import styled from "styled-components";
import { Data } from "./components/libraries/price_by_city";
import queryBackend from './backend/BackendQueryEngine';
import { Bar } from "react-chartjs-2";
import colorLib from '@kurkle/color';
import DynamicComponent from './DynamicComponent';
import Collapsible1Component from './Collapsible1Component';
import Collapsible2Component from './Collapsible2Component';
import Collapsible3Component from './Collapsible3Component';
import useDidMountEffect from './useDidMountEffect';
import mainlogo from './main-icon.svg';
import icon1 from './icon1.svg';
import icon3 from './icon3.svg';


const divStyle = {
  color: 'blue',
};
function App() {
  const [Cities, setCities] = useState([]);
  const [Cities2, setCities2] = useState([]);
  const [state1, setStateName] = useState("California");
  const [city1, setCityName] = useState("");
  const [city2, setCity2Name] = useState("");
  const [state2, setState2Name] = useState("Texas");
  const [globalProperty, setglobalProperty] = useState("Price");
  const [globalYear, setglobalYear] = useState("2022");
  const [predictionCity, setpredictionCity] = useState("");
  const [predictionYear, setpredictionYear] = useState("");
  const [plotcolor, setplotcolor] = useState("red");
  const ChildDiv = styled("div")`
   {
    display: inline-block;
    vertical-align: text-top;
    margin: 10 auto;
    gap: 50rem;
  }`;
  const CHART_COLORS = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
  };

  useEffect(() => {
    if (globalProperty=='Price'){
      setplotcolor("red")
    }else if(globalProperty=='PriceChange'){
      setplotcolor("orange")
    }else if(globalProperty=='Unemployment'){
      setplotcolor("purple")
    }else if(globalProperty=='Population'){
      setplotcolor("blue")
    }else if(globalProperty=='GDP'){
      setplotcolor("green")
    }else if(globalProperty=='Building Permits'){
      setplotcolor("brown")
    }
  }, [globalProperty]);


  function transparentize(value, opacity) {
    var alpha = opacity === undefined ? 0.5 : 1 - opacity;
    return colorLib(value).alpha(alpha).rgbString();
  }
  useEffect(() => {
    queryBackend(`get-cities?name=`+state1+`&year=`+globalYear+`&property=`+globalProperty).then((cities) => {
        setCities(cities);
    });

  }, [state1,globalYear,globalProperty]);
  useEffect(() => {
    queryBackend(`get-cities?name=`+state2+`&year=`+globalYear+`&property=`+globalProperty).then((cities2) => {
        console.log(cities2)
        setCities2(cities2);
    });

  }, [state2,globalYear,globalProperty]);
  const [StateData, setStateData] = useState({
    labels: Cities.map((data) => data.City),
    datasets: [
        {
            label: globalProperty,
            data: Cities.map((data) => data.value),
            fill: false, // use "True" to draw area-plot 
            borderColor: plotcolor,
            backgroundColor: transparentize(plotcolor, 0.5),
            pointBackgroundColor: 'black',
            pointBorderColor:'black'
        },
      ],}); 
      useEffect(() => { 
        setStateData({
              labels: Cities.map((data) => data.City),
              datasets: [
                  {
                      label: globalProperty,
                      data: Cities.map((data) => data.value),
                      fill: false, // use "True" to draw area-plot 
                      borderColor: plotcolor,
                      backgroundColor: transparentize(plotcolor, 0.5),
                      pointBackgroundColor: 'black',
                      pointBorderColor:'black'
                  },
                ],});
        
        }, [Cities]);
      const [State2Data, setState2Data] = useState({
        labels: Cities2.map((data) => data.City),
        datasets: [
            {
                label: globalProperty,
                data: Cities2.map((data) => data.value),
                fill: false, // use "True" to draw area-plot 
                borderColor: plotcolor,
                backgroundColor: transparentize(plotcolor, 0.5),
                pointBackgroundColor: 'black',
                pointBorderColor:'black'
            },
          ],}); 

    useEffect(() => { 
      setState2Data({
            labels: Cities2.map((data) => data.City),
            datasets: [
                {
                    label: globalProperty,
                    data: Cities2.map((data) => data.value),
                    fill: false, // use "True" to draw area-plot 
                    borderColor: plotcolor,
                    backgroundColor: transparentize(plotcolor, 0.5),
                    pointBackgroundColor: 'black',
                    pointBorderColor:'black'
                },
              ],});
      
      }, [Cities2]);
      
      useDidMountEffect(() => { 
        const anchor = document.querySelector('#prediction_div')
        anchor.scrollIntoView({ behavior: 'smooth', block: 'center' })
      }, [predictionYear]);


  return (
  <div>
    <div Style='margin: 0rem 0 0rem 0; display: flex;' >
    <div style={{
        width: '60vh',
        position: 'absolute', left: '50%', top: '10%',
        transform: 'translate(-50%, -50%)'
        }}>
          
        <h1> <img src={mainlogo} width={40} height={40} alt="logo"/> RealEstateGuru</h1>
        <h2>
            EXPLORE <img src={icon1} width={30} height={30} alt="logo"/>
          </h2>
      </div>
      
    <div style={{width:'75vh',
         position: 'absolute', left: '80%', top: '17%',
         transform: 'translate(-50%, -50%)'}}>
      <Collapsible1Component/>
    </div>

      <GeoWrapper setStateName={setStateName} setState2Name={setState2Name} setglobalYear={setglobalYear} 
      setglobalProperty={setglobalProperty}></GeoWrapper>

      <ChildDiv style={{ width:'80vh',
        position: 'absolute', left: '70%', top: '58%',
        transform: 'translate(-50%, -50%)'
        }}> 
      <h4>State 1: {state1}</h4>
      <Bar data={StateData} 
      options={{
        plugins: {
          legend:{display:false}
          },
        scales: { yAxes: {title: {
          display: true,
          text: function(){
            if (globalProperty=="Price"){return "Price ($)"}
            else if(globalProperty=="PriceChange"){return "Price w.r.t. 2022 (%)"}
            else if(globalProperty=="Unemployment"){return "Unemployment (%)"}
            else if(globalProperty=="GDP"){return "GDP (million $)"}
            else{return globalProperty}},
          font: {
            size: 12
            }
            }
            },  
         },
        aspectRatio:3.2,
        onClick: (event, elements) => {
          var datasetIndex=elements[0].index;
          var label = StateData.labels[datasetIndex];
          setCityName(label)
          const anchor = document.querySelector('#comparison')
          anchor.scrollIntoView({ behavior: 'smooth', block: 'center' })
        },
      } }
      />
      <h4> State 2: {state2}</h4>
     <Bar data={State2Data}
      options={{
        plugins: {
        legend:{display:false}
        },
        scales: { yAxes: {title: {
          display: true,
          text: function(){
            if (globalProperty=="Price"){return "Price ($)"}
            else if(globalProperty=="PriceChange"){return "Price w.r.t. 2022 (%)"}
            else if(globalProperty=="Unemployment"){return "Unemployment (%)"}
            else if(globalProperty=="GDP"){return "GDP (million $)"}
            else{return globalProperty}},
          font: {
            size: 12
            }
            }
            },
         },
        aspectRatio:3.2,
        onClick: (event, elements) => {
          var datasetIndex=elements[0].index;
          var label = State2Data.labels[datasetIndex];
          setCity2Name(label)
          const anchor = document.querySelector('#comparison')
          anchor.scrollIntoView({ behavior: 'smooth', block: 'center' })
        },
      } }
      />
      </ChildDiv> 
    </div>
      
    <div style={{width:'80vh',
         position: 'absolute', left: '80%', top: '105%',
         transform: 'translate(-50%, -50%)'}}>
      <Collapsible2Component/>
    </div>

    <div id='comparison' style={{width:'150vh',
        position: 'absolute', left: '45%', top: '115%',
        transform: 'translate(-50%, -50%)',
    }}>
      <ChartsComponent state1={state1} state2={state2} city1={city1} city2={city2} 
      setpredictionCity={setpredictionCity} setpredictionYear={setpredictionYear} setglobalProperty={setglobalProperty}>

      </ChartsComponent>
    </div>
    
    <div style={{width:'80vh',
         position: 'absolute', left: '80%', top: '221%',
         transform: 'translate(-50%, -50%)'}}>
      <Collapsible3Component/>
    </div>

    <div  style={{width:'92vh',
         position: 'absolute', left: '50%', top: '232%',
         transform: 'translate(-50%, -50%)'}}>
      <DynamicComponent  predictionCity={predictionCity} predictionYear={predictionYear}></DynamicComponent>
    </div>
    
  </div>

  );
}

export default App;
