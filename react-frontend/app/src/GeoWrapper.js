import React, { useState, useEffect } from 'react';
import './App.css';
import GeoComponent from './GeoComponent';
import ChartsComponent from './ChartsComponent';
import ReactTooltip from "react-tooltip";
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import fontawesome from '@fortawesome/fontawesome'
import {faCircleInfo } from '@fortawesome/free-solid-svg-icons'

fontawesome.library.add(faCircleInfo);

function GeoWrapper({setStateName,setState2Name,setglobalYear,setglobalProperty }) {
  const [year, setYear] = useState("2022");
  const [property, setProperty] = useState("Price");
  const [content, setContent] = useState("");
  const [descriptiontooltip, setdescriptiontooltip] = useState("");
  const [marks,setmarks]=useState([
    {
      value: 2000,
      label: '2000',
    },
    {
      value: 2040,
      label: '2040',
    },
  ])
  const [yearmin, setyearmin] = useState(2000);
  
  useEffect(() => { 
    setglobalYear(year)
    }, [year]);

    useEffect(() => { 
      setglobalProperty(property)
      }, [property]);
  
  function valuetext(value) {
    return `${value}`;
  }

  useEffect(() => {
    if(property=='Unemployment'){
      setmarks([{value: 2016,label: '2016',},
        { value: 2040,label: '2040', },]);
      setyearmin(2016)
      }
    else{
      setmarks( [{value: 2000,label: '2000',},
      { value: 2040,label: '2040', }]);
      setyearmin(2000)
    }
    if (property=='Price'){
      setdescriptiontooltip("Price: Smoothed, seasonally adjusted measure of the typical home value in USD")
    }else if(property=='PriceChange'){
      setdescriptiontooltip("Price % compared to the price in 2022")
    }else if(property=='Unemployment'){
      setdescriptiontooltip("Unemployment rate")
    }else if(property=='Population'){
      setdescriptiontooltip("Number of people living in the region")
    }else if(property=='GDP'){
      setdescriptiontooltip("Gross Domestic Product")
    }else if(property=='Building Permits'){
      setdescriptiontooltip("Number of building permits issued")
    }

  }, [property]);


  return (
    <div >
      
      <div style={{
        width: '70vh',
        position: 'absolute', left: '25%', top: '60%',
        transform: 'translate(-50%, -50%)'
        }}>
          <h2>
            United States of America
          </h2>
        <GeoComponent  setTooltipContent={setContent} year={year} property={property} setStateName={setStateName} setState2Name={setState2Name}></GeoComponent>
        <ReactTooltip >{content}</ReactTooltip>
      
      

      
      
      
        <h3>Select year: </h3>
      
        {/* <Box sx={{ width: 300 }}> */}
        <Slider
        value={year}
        onChange={event => setYear(event.target.value)}
        aria-label="Year"
        defaultValue={2022}
        getAriaValueText={valuetext}
        valueLabelDisplay="auto"
        step={1}
        min={yearmin}
        max={2040}
        marks={marks}
        // valueLabelDisplay="on"
        // https://mui.com/material-ui/react-slider/

        ></Slider>

        <div style={{ display:'inline-block'}}>
        <FontAwesomeIcon icon="fa-solid fa-circle-info" title={descriptiontooltip} 
        /><h3> Select indicator: </h3>
        </div>

        <div>
        <select data-tip data-for="optiontip"
        value={property}
        onChange={event => setProperty(event.target.value)}>
        <option value="Price">Price ($)</option>
        <option value="PriceChange">Price w.r.t 2022 (%)</option>
        <option value="Population">Population (count)</option>
        <option value="Unemployment">Unemployment (%)</option>
        <option value="GDP">Annually GDP (million $)</option>
        <option value="Building Permits">Building Permits (count)</option>
        </select>
        
        </div>
      </div>
    </div>




  );
}

export default GeoWrapper;
