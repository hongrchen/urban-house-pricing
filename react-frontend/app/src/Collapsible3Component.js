import React from 'react';
import useCollapse from 'react-collapsed';
import './App.css';

const Collapsible3Component = () => {

    const config = {
        duration: 1000
    };
    const { getCollapseProps, getToggleProps, isExpanded } = useCollapse(config);
return (
    <div className="collapsible">
        <div className="header" {...getToggleProps()}>
        {isExpanded ? '' : 'Section 3 '}
        <span style={{ color:'black', fontWeight: 'bold'}}>
            {isExpanded ? 'Hide' : 'PREDICT'}
        </span>
        {isExpanded ? '' : ': (Click to expand)'}
        </div>
        <div {...getCollapseProps()}>
        <div className="content" style={{textAlign: 'center'}}>
                Now you can make your personalized predictions! <br></br>
                Fill in ALL the text boxes with your estimations and ... Get predictions!
                <br></br>
                </div>
                <div className="content" style={{textAlign: 'left'}}>
                <span style={{ color:'black', fontWeight: 'bold', textAlign: 'left'}}>
                Tips: 
            </span> Click on a point in the line chart in Section 2 to see the estimated indicators' values for the selected year.
            </div>
        </div>
    </div>
    );
};






export default Collapsible3Component;
