import { JsonDecoder } from 'ts.data.json';
import { DataPoint } from '../types/DataPoint';

/* **********
 * number[] *
 ********** */
const dataPointDecoder = JsonDecoder.object<DataPoint>(
    {
        year: JsonDecoder.number,
        price: JsonDecoder.number,
        City: JsonDecoder.string,
    },
    'DataPoint'
);

/* ***********
 * DataArray *
 *********** */
export const dataArrayDecoder = JsonDecoder.array<DataPoint>(dataPointDecoder, 'DataArray');
