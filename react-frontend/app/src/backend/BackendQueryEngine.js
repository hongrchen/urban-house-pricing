
export const BASE_URL = 'http://127.0.0.1:8000';

export const queryBackend = async (route) => {
    const requestURL = `${BASE_URL}/${route}`;
    // const formData = new FormData();
    const data = await fetch(requestURL,
        {
            method: 'POST'
        }
    ).then(response => response.json());

    return data;
}


export default queryBackend;


