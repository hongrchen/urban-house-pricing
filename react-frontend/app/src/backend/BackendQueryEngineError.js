
export const BASE_URL = 'http://127.0.0.1:8000';

export const queryBackendHandleError = async (route) => {
    const requestURL = `${BASE_URL}/${route}`;
    // const formData = new FormData();
    const data = await fetch(requestURL,
        {
            method: 'POST',
        }
    ).then(response => response.json());
    if (data.length>0) {
        // You can do your error handling here
        //const res = await data.json()
        return data
    } else {
        // Call the .json() method on your response to get your JSON data
        return {'error':true}
    }

}



export default queryBackendHandleError;


