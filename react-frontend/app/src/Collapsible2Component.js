import React from 'react';
import useCollapse from 'react-collapsed';
import './App.css';

const Collapsible2Component = () => {

    const config = {
        duration: 1000
    };
    const { getCollapseProps, getToggleProps, isExpanded } = useCollapse(config);
return (
    <div className="collapsible">
        <div className="header" {...getToggleProps()}>
        {isExpanded ? '' : 'Section 2 '}
        <span style={{ color:'black', fontWeight: 'bold'}}>
            {isExpanded ? 'Hide' : 'COMPARE'}
        </span>
        {isExpanded ? '' : ': (Click to expand)'}
        </div>
        <div {...getCollapseProps()}>
            <div className="content" style={{textAlign: 'center'}}>
            Choose two cities from the dropdown buttons and COMPARE the house prices and socio-economic indicators shown on the line charts.
            <br></br>
            </div>
            <div className="content" style={{textAlign: 'left'}}>
            <span style={{ color:'black', fontWeight: 'bold', textAlign: 'left',marginRight: 'auto'}}>
                Tips: 
            </span> Click one point in the line charts to start the personalized predictions.
            </div>
        </div>
    </div>
    );
};






export default Collapsible2Component;
