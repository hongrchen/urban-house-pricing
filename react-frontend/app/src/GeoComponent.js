import React, { useState, useEffect } from "react";
import { ComposableMap, Geographies, Geography } from "react-simple-maps";
import { scaleQuantize } from "d3-scale";
import {scaleLinear } from "d3";
import queryBackend from './backend/BackendQueryEngine';
import {Colorscale} from 'react-colorscales';
import {
  ZoomableGroup
} from "react-simple-maps";
//const geoUrl = "https://cdn.jsdelivr.net/npm/us-atlas@3/states-10m.json";
const geoUrl = 'states-10m.json';
const colorScale = scaleQuantize()
  .domain([1, 10])
  .range([
    "#ffedea",
    "#ffcec5",
    "#ffad9f",
    "#ff8a75",
    "#ff5533",
    "#e2492d",
    "#be3d26",
    "#9a311f",
    "#782618"
  ]);

const GeoComponent = ({ setTooltipContent,year,property,setStateName,setState2Name }) => {
  
    const [data, setData] = useState([]);
    const [order, setOrder] = useState({ord:0});
    //var order=0
    const [name, setName] = useState('Texas');
  
  //useEffect(() => {
  //   https://www.bls.gov/lau/
  //  csv("/Unemployment-by-county-2017.csv").then(counties => {
  //    setData(counties);
  //  });
  //}, []);

  useEffect(() => {
    queryBackend(`get-data?year=`+year+`&pro=`+property).then((states) => {
        setData(states);
    });

  }, [property,year]);

  useEffect(() => {
    console.log(order.ord)
    if(order.ord%2==0){
      setState2Name(name);
    }else{
      setStateName(name);
    }

  }, [order]);




// dynamic color scaling of map
const [minvalue, setmin] = useState([]);
const [maxvalue, setmax] = useState([]);
const [color, setcolor] = useState("red");

useEffect(() => {
  queryBackend(`get-minVal?year=`+year+`&pro=`+property).then((states) => {
    setmin(Math.round(states));
  });
  queryBackend(`get-maxVal?year=`+year+`&pro=`+property).then((states) => {
    if(states<1000){
      setmax(Math.round(states/10)*10)
    }else if (states<10000){
      setmax(Math.round(states/100)*100)
    }else if (states<100000){
      setmax(Math.round(states/1000)*1000)
    }else if (states<1000000){
      setmax(Math.round(states/10000)*10000)
    }else if (states<10000000){
      setmax(Math.round(states/100000)*100000)
    }else if (states<100000000){
      setmax(Math.round(states/1000000)*1000000)
    }else if (states<1000000000){
      setmax(Math.round(states/10000000)*10000000)
    }
  });
  if (property=='Price'){
    setcolor("red")
  }else if(property=='PriceChange'){
    setcolor("orange")
  }else if(property=='Unemployment'){
    setcolor("purple")
  }else if(property=='Population'){
    setcolor("blue")
  }else if(property=='GDP'){
    setcolor("green")
  }else if(property=='Building Permits'){
    setcolor("brown")
  }

}, [property,year]);


const colorScale = scaleLinear().domain([minvalue, maxvalue]).range(["white", color]);



var counter=0
  return (
    <>


    <div Style={'width:90vh', 'display:flex'}>
    <table >
        <tbody>

              <tr>
              <td style={{fontSize:10}}> {maxvalue.toLocaleString()}</td>
                <td style={{backgroundColor:colorScale(maxvalue),color:colorScale(maxvalue),border: '1px solid black'}}> a</td>
              </tr>
              <tr>
              <td style={{fontSize:10}}> {(maxvalue*0.75).toLocaleString()}</td>
              <td style={{backgroundColor:colorScale(maxvalue*0.75),color:colorScale(maxvalue*0.75),border: '1px solid black'}}> a</td>
              </tr>
              <tr>
              <td style={{fontSize:10}}> {(maxvalue*0.5).toLocaleString()}</td>
                <td style={{backgroundColor:colorScale(maxvalue/2),color:colorScale(maxvalue/2),border: '1px solid black'}}> a</td>
              </tr>
              <tr>
              <td style={{fontSize:10}}> {(maxvalue*0.25).toLocaleString()}</td>
              <td style={{backgroundColor:colorScale(maxvalue*0.25),color:colorScale(maxvalue*0.25),border: '1px solid black'}}> a</td>
              </tr>
              <tr>
              <td style={{fontSize:10}}> {minvalue.toLocaleString()}</td>
              <td style={{backgroundColor:colorScale(minvalue),color:colorScale(minvalue),border: '1px solid black'}}> a</td>
              </tr>
        </tbody>
      </table>
      <ComposableMap data-tip="" projection="geoAlbersUsa">
        <ZoomableGroup zoom={1} maxZoom={11}>
            <Geographies geography={geoUrl}>
            {({ geographies }) =>
                geographies.map(geo => {
                const cur = data.find(s => s.RegionName === geo.properties.name);
                return (
                    <Geography
                    key={geo.rsmKey}
                    geography={geo}
                    onClickCapture={() => {
                      setOrder({ord:order.ord+1})
                      setName(`${cur.RegionName}`);
                      
                    }}
                    
                    onMouseEnter={() => {
                        setTooltipContent(`${cur.RegionName} — ${cur.value}`);
                      }}
                      onMouseLeave={() => {
                        setTooltipContent("");
                      }}
                      style={{
                        hover: {
                          fill: "blue",
                          outline: "none"
                        },
                        pressed: {
                          fill: "#E42",
                          outline: "none"
                        }
                      }}
                    fill={colorScale(cur ? cur.value : "#EEE")}
                    />
                );
                })
            }
            </Geographies>
        </ZoomableGroup>
      </ComposableMap>

      </div>
      <div Style={'width:60vh'}>

</div>
    </>
  );
};

export default GeoComponent;
