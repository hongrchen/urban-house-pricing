import React from 'react';
import useCollapse from 'react-collapsed';
import './App.css';

const Collapsible1Component = () => {

    const config = {
        duration: 1000
    };
    const { getCollapseProps, getToggleProps, isExpanded } = useCollapse(config);
return (
    <div className="collapsible">
        <div className="header" {...getToggleProps()}>
        {isExpanded ? '' : 'Section 1 '}
        <span style={{ color:'black', fontWeight: 'bold'}}>
            {isExpanded ? 'Hide' : 'EXPLORE'}
        </span>
        {isExpanded ? '' : ': (Click to expand)'}
        </div>
        <div style={{backgroundColor:'blue'}} {...getCollapseProps()}>
            <div className="content" style={{textAlign: 'center'}}>
                <span>
                    Explore the properties in the map. Click two states to show the statistics on the bar charts on the right.  
                </span>
                <br></br>
                </div>
                <div className="content" style={{textAlign: 'left'}}>
                <span style={{ color:'black', fontWeight: 'bold'}}>
                    Tips: 
                </span> Click one city from each bar chart to get an in-depth analysis.
            </div>
        </div>
    </div>
    );
};






export default Collapsible1Component;
