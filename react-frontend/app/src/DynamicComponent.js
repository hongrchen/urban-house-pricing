import React, { useState, useEffect } from "react";
import { scaleQuantize } from "d3-scale";
import {scaleLinear } from "d3";
import queryBackend from './backend/BackendQueryEngine';
import queryBackendHandleError from './backend/BackendQueryEngineError';
import styled from "styled-components";
import colorLib from '@kurkle/color';
import BarChart from "./components/libraries/BarChart";
import LineChart from "./components/libraries/LineChart";
import useDidMountEffect from './useDidMountEffect';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import fontawesome from '@fortawesome/fontawesome'
import { faInfo } from '@fortawesome/free-solid-svg-icons'
import icon3 from './icon3.svg';

fontawesome.library.add(faInfo);
    // Set input field style
    const StyledContainer = styled.div`
        position: relative;
        align-items: center;
        width: 90%;
        border-radius: 8 8 0 0;
        background-color: #eff2f7;
    `;

    const StyledInnerLabel = styled.p`
        position: absolute;
        top: -15px;
        color: brown;

        &.prefix {
            max-width: 230px;
            padding-left: 20px;
        }
        &.units {
          max-width: fit-content;
          padding-left: 20px;
          left: 62%;
      }
        &.results {
          max-width: 200px;
          padding-left: 5px;
          padding-right: 100px;
      }
        &.suffix {
            max-width: fit-content;
            padding-right: 20px;
            right: 0;
        }
`   ;

    const StyledInput = styled.input`
        width: 20%;
        background-color: none;
        outline: none;
        line-height: 2;
        padding-right: 20px;
        padding-left: 10px;
        &.results_input {
          width: 20%;
          left: 100%;
          margin-left: 20px;
          padding-right: 1px;
          
      }
      &:invalid {
        background-color:#f1a1a1;
        
    }
    `;
    const Button = styled.button`
    background-color: #71aef3;
    color: white;
    font-size: 20px;
    padding: 5px 5px;
    border-radius: 5px;
    margin: 0px 0px;
    cursor: pointer;
    &:active {
      background-color: #5786bb;
    }
  `;

const DynamicComponent = ({predictionCity,predictionYear}) => {
    function transparentize(value, opacity) {
        var alpha = opacity === undefined ? 0.5 : 1 - opacity;
        return colorLib(value).alpha(alpha).rgbString();
      }
      const CHART_COLORS = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(201, 203, 207)'
      };
    // Define city input filled
    
    const [CurrentData, setCurrentData] = useState([{'City':'','Year':'','Population':'','Unemployment':'','GDP':'','Building Permits':'','Interest rate':''}]);

    const [city,setCity] = useState("Dallas");
    const [year,setYear] = useState(null);

    // Define population input filed
    const [population,setPopulation] = useState(null);
    const [permits,setPermits] = useState(null);
    const [gdp,setGDP] = useState(null);
    const [interest,setInterest] = useState(null);
    const [unemployment,setUnemployment] = useState(null);

    const [predictioncity,setpredictionCity] = useState("");
    const [predictionyear,setpredictionYear] = useState("");
    // Define indicator input filed
    const [predictionpopulation,setpredictionPopulation] = useState("");
    const [predictionpermits,setpredictionPermits] = useState("");
    const [predictiongdp,setpredictionGDP] = useState("");
    const [predictioninterest,setpredictionInterest] = useState("");
    const [predictionunemployment,setpredictionUnemployment] = useState("");

    // Define previous indicator input filed
    const [pastpredpopulation,setpastpredPopulation] = useState("");
    const [pastpredpermits,setpastpredPermits] = useState("");
    const [pastpredgdp,setpastpredGDP] = useState("");
    const [pastpredinterest,setpastpredInterest] = useState("");
    const [pastpredunemployment,setpastpredUnemployment] = useState("");

    const options ={plugins: {
        title: {
            display: true,
            text: 'Dynamic House Price Prediction'
              }
          },
        scales: { yAxes: {title: {
            display: true,
            text: "Price ($)",
            font: {
                size: 13
            }
            }
         },
                xAxes: {title: {
                    display: true,
                    text: "Year",
                    font: {
                        size: 13
                    }
                }
              },  
            }
        }



    const [predictions_data, setPredictions_data] = useState([]);
    const [Request, setRequest] = useState(false);
    useDidMountEffect(() => {
        if (true) {
            if (year<2023){
                setYear(2023);
                var year_request=2023
            }else if (year>2100){
              setYear(2100);
              var year_request=2100
            }else {
                var year_request=year
            }
            
            if (year>2100){
                setYear(2100);
                var year_request=2100
            }else{
                var year_request=year
            }

            queryBackendHandleError(`get-dynamic-prices?city=`+city+`&year=`+year_request+`&population=`+population+`&unemployment=`+unemployment+`&gdp=`+gdp+`&permits=`+permits+`&interest=`+interest).then((data) => {
              
              if (!('error' in data)){
                setPredictions_data(data);
                setpredictionYear(year_request)
                setpredictionCity(city)
                setpredictionPopulation(population)
                setpredictionUnemployment(unemployment)
                setpredictionGDP(gdp)
                setpredictionPermits(permits)
                setpredictionInterest(interest)
                //console.log(data)
                //console.log(data.slice(-1)[0])
                //console.log(data.slice(-1)[0]['pop'])
                if (data.slice(-1)[0]['pop'] != null){
                  setpastpredPopulation(data.slice(-1)[0]['pop'])
                  setpastpredUnemployment(data.slice(-1)[0]['unemp'])
                  setpastpredGDP(data.slice(-1)[0]['gdp'])
                  setpastpredPermits(data.slice(-1)[0]['b_perm'])
                  setpastpredInterest(data.slice(-1)[0]['int'])
                }
              } 
              
            });
            setRequest(false)
            
        }
      }, [Request]);

      const [predictions, setPredictions] = useState({
        labels: predictions_data.map((data) => data.year),
        datasets: [
            {
                label:"Current prediction",
                data: predictions_data.map((data) => data.price),
                fill: false, // use "True" to draw area-plot 
                borderColor: CHART_COLORS.red,
                backgroundColor: transparentize(CHART_COLORS.red, 0.5),
                pointBackgroundColor: 'black',
                pointBorderColor:'black'
            },
            {
                label:"Lower limit",
                data: predictions_data.map((data) => data.price_min),
                fill: 0, // use "True" to draw area-plot 
                borderColor: CHART_COLORS.red,
                backgroundColor: transparentize(CHART_COLORS.red, 0.5),
                pointBackgroundColor: 'transparent',
                pointBorderColor:'transparent'
            },
            {
                label:"Upper limit",
                data: predictions_data.map((data) => data.price_max),
                fill: 0, // use "True" to draw area-plot 
                borderColor: CHART_COLORS.red,
                backgroundColor: transparentize(CHART_COLORS.red, 0.5),
                pointBackgroundColor: 'transparent',
                pointBorderColor:'transparent'
            },
            {
                label:"Previous prediction",
                data: predictions_data.map((data) => data.prev_price),
                fill: 0, // use "True" to draw area-plot 
                borderColor: CHART_COLORS.blue,
                backgroundColor: transparentize(CHART_COLORS.blue, 1.0),
                borderDash: [5, 5],
                pointBackgroundColor: 'transparent',
                pointBorderColor:'transparent'
            }
          ]})
          useDidMountEffect(() => { 
        setPredictions({
            labels: predictions_data.map((data) => data.year),
            datasets: [
                {
                    label: "Current prediction",
                    data: predictions_data.map((data) => data.price),
                    fill: false, // use "True" to draw area-plot 
                    borderColor: CHART_COLORS.red,
                    backgroundColor: transparentize(CHART_COLORS.red, 0.5),
                    pointBackgroundColor: 'black',
                    pointBorderColor:'black'
                },
                {
                    label: "Lower bound",
                    data: predictions_data.map((data) => data.price_min),
                    fill: 0, // use "True" to draw area-plot 
                    borderColor: CHART_COLORS.red,
                    backgroundColor: transparentize(CHART_COLORS.red, 0.8),
                    pointBackgroundColor: 'transparent',
                    pointBorderColor:'transparent'
                },
                {
                    label: "Upper bound",
                    data: predictions_data.map((data) => data.price_max),
                    fill: 0, // use "True" to draw area-plot 
                    borderColor: CHART_COLORS.red,
                    backgroundColor: transparentize(CHART_COLORS.red, 0.8),
                    pointBackgroundColor: 'transparent',
                    pointBorderColor:'transparent'
                },
                {
                    label: "Previous prediction",
                    data: predictions_data.map((data) => data.prev_price),
                    fill: 0, // use "True" to draw area-plot 
                    borderColor: CHART_COLORS.blue,
                    backgroundColor: transparentize(CHART_COLORS.blue, 1.0),
                    borderDash: [5, 5],
                    pointBackgroundColor: 'transparent',
                    pointBorderColor:'transparent'
                }
              ]});
      console.log("update")
      }, [predictions_data]);

      const handleCityChange = (e) => {
        e.preventDefault(); // prevent the default action
        setCity(e.target.value) // set name to e.target.value (event)
      };
      const handleYearChange = (e) => {
        e.preventDefault(); // prevent the default action
        setYear(e.target.value) // set name to e.target.value (event)
      };
      const handlePopulationChange = (e) => {
        e.preventDefault(); // prevent the default action
        setPopulation(e.target.value) // set name to e.target.value (event)
      };
      const handleUnemploymentChange = (e) => {
        e.preventDefault(); // prevent the default action
        setUnemployment(e.target.value) // set name to e.target.value (event)
      };
      const handleGDPChange = (e) => {
        e.preventDefault(); // prevent the default action
        setGDP(e.target.value) // set name to e.target.value (event)
      };
      const handleInterestChange = (e) => {
        e.preventDefault(); // prevent the default action
        setInterest(e.target.value) // set name to e.target.value (event)
      };
      const handlePermitsChange = (e) => {
        e.preventDefault(); // prevent the default action
        setPermits(e.target.value) // set name to e.target.value (event)
      };
      
      useDidMountEffect(() => {
        if (predictionYear<2023){
            setYear(2023);
            var year_request=2023
        }else{
            setYear(predictionYear);
            var year_request=predictionYear
        }
        setCity(predictionCity);
        queryBackend(`get-placeholders?city=`+predictionCity+`&year=`+year_request).then((data) => {
            setCurrentData(data);
        });

    }, [predictionCity,predictionYear]);

    useDidMountEffect(() => {
        setPopulation(CurrentData[0].Population)
        setUnemployment(CurrentData[0].Unemployment)
        setGDP(CurrentData[0].GDP)
        setPermits(CurrentData[0]['Building Permits'])
        setInterest(CurrentData[0]['Interest rate'])

    }, [CurrentData]);


  return (
<div>
    <div>
      <h2>PREDICT <img src={icon3} width={30} height={30} alt="logo"/>  
        <br/>
        Create Your Personalized Prediction!
        <br/>
      </h2>



      <div style={{width:'85vh',
        position: 'absolute', left: '50%', top: '280%',
        transform: 'translate(-50%, -50%)'
        }}>
      {/* City filed */}
      <div>
        <StyledContainer>
            <StyledInnerLabel className="prefix">City </StyledInnerLabel>{' '}
            <StyledInput required type="text" value={city} onChange={handleCityChange}/>
            <StyledInnerLabel className="suffix"> e.g. Dallas</StyledInnerLabel>
            
        </StyledContainer>
      </div>
 

      {/* Year filed */}
      <div>
        <StyledContainer>
            <StyledInnerLabel className="prefix">Year </StyledInnerLabel>{' '}
            <StyledInput required type="text" value={year} onChange={handleYearChange}/>
            <StyledInnerLabel className="suffix"> e.g. 2030</StyledInnerLabel>
        </StyledContainer>
      </div>

        {/* Population filed */}
      <div>
        <StyledContainer>
            <StyledInnerLabel className="prefix">Population (#) </StyledInnerLabel>{' '}
            <StyledInput required type="text" value={population} onChange={handlePopulationChange}/>
            <StyledInnerLabel className="suffix"> e.g. 1000000</StyledInnerLabel>
        </StyledContainer>
      </div>

      <div>
        <StyledContainer>
            <StyledInnerLabel className="prefix">GDP (million $) </StyledInnerLabel>{' '}
            <StyledInput required type="text" value={gdp} onChange={handleGDPChange}/>
            <StyledInnerLabel className="suffix"> e.g. 4000000</StyledInnerLabel>
        </StyledContainer>
      </div>

      <div>
        <StyledContainer>
            <StyledInnerLabel className="prefix">Building permits (#) </StyledInnerLabel>{' '}
            <StyledInput required type="number" value={permits} onChange={handlePermitsChange}/>
            <StyledInnerLabel className="suffix"> e.g. 200</StyledInnerLabel>
        </StyledContainer>
      </div>

      <div>
        <StyledContainer  id='prediction_div'>
            <StyledInnerLabel className="prefix">Interest rate (%) </StyledInnerLabel>{' '}
            <StyledInput required type="number" value={interest} onChange={handleInterestChange}/>
            <StyledInnerLabel className="suffix"> e.g. 5.1</StyledInnerLabel>
        </StyledContainer>
      </div>

      <div>
        <StyledContainer>
            <StyledInnerLabel className="prefix">Unemployment rate (%)</StyledInnerLabel>{' '}
            <StyledInput required type="text" value={unemployment} onChange={handleUnemploymentChange}/>
            <StyledInnerLabel className="suffix"> e.g. 4.8</StyledInnerLabel>
        </StyledContainer>
      
      </div>

      <br />
        <Button onClick={()=>setRequest(true)}> Predict ! </Button>
      <br /><br />
        
      
      </div>
      <div style={{width:'65vh',
        position: 'absolute', left: '25%', top: '630%',
        transform: 'translate(-80%, -50%)'
      }}>
            

        <div style={{width:'60vh',
          position: 'absolute', left: '45%', top: '0%',
          transform: 'translate(-50%, -50%)'
          }}>
                
          <h4>Prediction information (current and previous predictions)</h4>
             
        </div>
            <br />
            <div>
                <StyledContainer>
                <StyledInnerLabel className="results">City: </StyledInnerLabel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <StyledInput className="results_input" type="text" size="20" disabled value={predictioncity} onChange={handleYearChange}/>
                </StyledContainer>
            </div>
           
            <div>
                <StyledContainer>
                <StyledInnerLabel className="results">Year: </StyledInnerLabel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <StyledInput className="results_input" type="text" disabled value={predictionyear} onChange={handleYearChange}/>
                </StyledContainer>
            </div>
            
            <div>
                <StyledContainer>
                <StyledInnerLabel className="results">Population (#) :  </StyledInnerLabel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <StyledInput className="results_input" type="text" disabled value={predictionpopulation} onChange={handleYearChange}/>
                <StyledInput className="results_input" type="text" disabled value={pastpredpopulation} onChange={handleYearChange}/>
                </StyledContainer>
            </div>
            
            <div>
                <StyledContainer>
                <StyledInnerLabel className="results">GDP (million $):  </StyledInnerLabel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <StyledInput className="results_input" type="text" disabled value={predictiongdp} onChange={handleYearChange}/>
                <StyledInput className="results_input" type="text" disabled value={pastpredgdp} onChange={handleYearChange}/>
                </StyledContainer>
            </div>
            
            <div>
                <StyledContainer>
                <StyledInnerLabel className="results">Building permits (#):  </StyledInnerLabel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <StyledInput className="results_input" type="text" disabled value={predictionpermits} onChange={handleYearChange}/>
                <StyledInput className="results_input" type="text" disabled value={pastpredpermits} onChange={handleYearChange}/>
                </StyledContainer>
            </div>
           
            <div>
                <StyledContainer>
                <StyledInnerLabel className="results">Interest rate (%):  </StyledInnerLabel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <StyledInput className="results_input" type="text" disabled value={predictioninterest} onChange={handleYearChange}/>
                <StyledInput className="results_input" type="text" disabled value={pastpredinterest} onChange={handleYearChange}/>
                </StyledContainer>
            </div>
            
            <div>
                <StyledContainer>
                <StyledInnerLabel className="results">Unemployment rate (%):  </StyledInnerLabel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <StyledInput className="results_input" type="text" disabled value={predictionunemployment} onChange={handleYearChange}/>
                <StyledInput className="results_input" type="text" disabled value={pastpredunemployment} onChange={handleYearChange}/>
                </StyledContainer>
            </div>
            
        </div>

        <div style={{width:'100vh',
        position: 'absolute', left: '90%', top: '650%',
        transform: 'translate(-50%, -50%)'
        }}>
           <LineChart chartData={predictions} options={options}/>
        </div>

    </div>
</div>   
  );
};




export default DynamicComponent;